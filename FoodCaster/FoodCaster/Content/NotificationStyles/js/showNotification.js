﻿function showNotification(messageText) {
    var notification = new NotificationFx({
        message: messageText,
        layout: 'growl',
        effect: 'genie',
        type: 'notice',
        ttl: 3000
    });
    notification.show();
}