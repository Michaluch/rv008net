﻿AdminModule.controller('UsersController', ['$scope', '$http', function ($scope, $http) {
    //List of users. Get from the server
    $scope.users = {};

    //Array for saving users in one page
    $scope.items = {};

    //For show userlist count
    $scope.TotalUsers = 0;

    //Data for search users
    $scope.searchData = {};

    //Current user data for add delete and edit
    $scope.userData = {};

    //Options for set items count in one page
    $scope.countVariants = [10, 20, 30, 50];

    //Set count of items in one page
    $scope.countItems = 10;

    //Set default current page
    $scope.currentPage = 0;

    //Set default (how many pages)
    $scope.pageCount = 5;

    //Set max count of pagination links list
    $scope.viewLinks = 5;

    //Last page number for button "Last>>"
    $scope.lastPage = 10;

    //Get data from server after load all html page and update angular model
    var result = $http.post("/Admin/UsersAjax", {model : null});
    result.success(function (data) {
        $scope.users = data.UsersList;
        $scope.roles = data.RolesList;  
        $scope.RebuildPages();
    });

    //Rebuilding page for show new information
    $scope.RebuildPages = function () {
        $scope.TotalUsers = $scope.users.length;
        $scope.countItems = 10;
        if ($scope.countItems > $scope.TotalUsers) {
            $scope.countItems = $scope.TotalUsers;
        }
        $scope.items = {};
        for (var i = 0; i < $scope.countItems; i++) {
            $scope.items[i] = $scope.users[i];
        }
        $scope.lastPage = Math.ceil(($scope.users.length) / $scope.countItems) - 1;
        $scope.setPage($scope.currentPage);
        $scope.searchData = {};
    }

    //Find user in server
    $scope.FindUsers = function () {
        $http.post("/Admin/UsersAjax", {
        
            Role: $scope.searchData.Role,
            Email: $scope.searchData.Email,
            UserName: $scope.searchData.Name,
    
            })
            .success(function (data) {
                $scope.users = data.UsersList;
                $scope.RebuildPages();
            });
    };

    //Use after click edit button for show current iser information
    $scope.setUserDataById = function (id) {
        for (var i = 0; i < $scope.users.length; i++) {
            if ($scope.users[i].Id == id) {
                $scope.userData.UserName = $scope.users[i].UserName;
                $scope.userData.Email = $scope.users[i].Email;
                $scope.userData.Id = id;
                $scope.userData.Role = $scope.users[i].Role;
                break;
            }
        }
    }

    //Show modal window for edit user data
    $scope.ShowEditModal = function (id) {
        $scope.userData = {};
        $scope.setUserDataById(id);
        $('#ModalTitle').html("Edit user data (user id =" + id + ")");
        $('#Action').html("Change user data");
        $("div.error").attr("class", "small-5 small-pull-3 columns");
        $("label.error").attr("class", "right inline");
        $('#modalAddUser').foundation('reveal', 'open');
    };

    //Show modal window for add new user data
    $scope.ShowAddModal = function () {
        $scope.userData = {};
        $('#ModalTitle').html("Add new user data");
        $('#Action').html("Add user data");
        $("div.error").attr("class", "small-5 small-pull-3 columns");
        $("label.error").attr("class", "right inline");
        $('#modalAddUser').foundation('reveal', 'open');
        $scope.userData.Role = "User";
    };

    //Press Ok in modal window
    $scope.ModalOk = function () {
        var messageText = "";
        if ($("div.error").length == 0) {
            if ($scope.userData.confirmPass == $scope.userData.Pass) {
                if ($scope.userData.Id == null) {
                    if ($scope.userData.Pass.length >= 8) {
                        $.post("/Admin/AddNewUser",
                            {
                                "UserName": $scope.userData.UserName,
                                "Email": $scope.userData.Email,
                                "Role": $scope.userData.Role,
                                "Password": CryptoJS.SHA512($scope.userData.Pass).toString(CryptoJS.enc.Hex),
                            })
                            .done(function (data) {
                                
                                if (Boolean(data.success)) {
                                    $scope.users.push(data.addedUser);
                                    $scope.RebuildPages();
                                    $('#modalAddUser').foundation('reveal', 'close');
                                    messageText = '<p><h4 class="notificationSuccessTitle">Added</h4></p><p>' + data.message + '</p>';
                                } else {
                                    messageText = '<p><h4 class="notificationErrorTitle">Error</h4></p><p>' + data.message + '</p>';
                                };
                                showNotification(messageText);
                                $scope.$apply();
                            });
                    };
                } else {
                    shaString = "";
                    if ($scope.userData.Pass != null)
                        var shaString = CryptoJS.SHA512($scope.userData.Pass).toString(CryptoJS.enc.Hex);
                    $.post("/Admin/ChangeUserData",
                         {
                             "Id": $scope.userData.Id,
                             "UserName": $scope.userData.UserName,
                             "Email": $scope.userData.Email,
                             "Role": $scope.userData.Role,
                             "Password": shaString,
                         })
                         .done(function (data) {       
                             if (Boolean(data.success)) {
                                 $('#modalAddUser').foundation('reveal', 'close');
                                 for (var i = 0; i < $scope.users.length; i++) {
                                     if ($scope.users[i].Id == $scope.userData.Id) {
                                         $scope.users[i].UserName = $scope.userData.UserName;
                                         $scope.users[i].Email = $scope.userData.Email;
                                         $scope.users[i].Role = $scope.userData.Role;
                                         break;
                                     };
                                 };
                                 $scope.setPage($scope.currentPage);
                                 messageText = '<p><h4 class="notificationSuccessTitle">Changed</h4></p><p>' + data.message + '</p>';
                             } else {
                                 messageText = '<p><h4 class="notificationErrorTitle">Error</h4></p><p>' + data.message + '</p>';
                             };
                             showNotification(messageText);
                             $scope.$apply();
                         });
                };
            };
        } else {
            messageText = '<p><h4 class="notificationErrorTitle">Error</h4></p><p>Data in form is not valid</p>';
            showNotification(messageText);
        };
    };

    //Delete user reqest
    $scope.Delete = function (id) {
        $.post("/Admin/UserDelete", {jsonUser: { Id: id } })
            .done(function (data) {
                var messageText = '';
                if (Boolean(data.success)) {
                    $('#modalAddUser').foundation('reveal', 'close');
                    for (var i = 0; i < $scope.users.length; i++) {
                        if ($scope.users[i].Id == id) {
                            $scope.users.splice(i, 1); break;
                        }  
                    }
                    $scope.setPage($scope.currentPage);
                    messageText = '<p><h4 class="notificationSuccessTitle">Deleted</h4></p><p>' + data.message + '</p>';
                } else {
                    messageText = '<p><h4 class="notificationErrorTitle">Error</h4></p><p>' + data.message + '</p>';
                };
                showNotification(messageText);
                $scope.$apply();
            });
    };

    //Get pagination links int array (1,2,3..)
    $scope.range = function () {
        var rangeSize = $scope.viewLinks;
        var ret = [];
        var start = 0;
        if ($scope.currentPage > $scope.viewLinks - 2) {
            start = $scope.currentPage;
        }
        for (var i = start; i < start + rangeSize; i++) {
            if (i > ($scope.users.length - 1) / $scope.countItems) continue;
            ret.push(i);
        }
        $scope.pageCount = ret.length - 1;
        return ret;
    };

    //Show current page + next page and prev page
    $scope.setPage = function (n) {
        $scope.lastPage = Math.ceil(($scope.users.length) / $scope.countItems) - 1;
        if (n >= 0) {
            if (n > $scope.lastPage) { $scope.currentPage = $scope.lastPage; }
            else { $scope.currentPage = n; }  
            $scope.items = {};
            for (var i = 0; i < $scope.countItems; i++) {
                if ($scope.users[i + $scope.currentPage * $scope.countItems] == null) continue;
                $scope.items[i] = $scope.users[i + $scope.currentPage * $scope.countItems];
            }
            
        };
    };

    //Go in prev page after click < button
    $scope.prevPage = function () {
        if ($scope.currentPage > 0) {
            $scope.currentPage--;
            $scope.setPage($scope.currentPage);
        }
    };

    //Go in next page after click > button
    $scope.nextPage = function () {
        if ($scope.currentPage < $scope.pageCount) {
            $scope.currentPage++;
            $scope.setPage($scope.currentPage);
        }
    };
}]);



