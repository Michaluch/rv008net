﻿
var AdminModule = angular.module('AdminModule', []);
//Admin route configuration
AdminModule.config(['$routeProvider', '$locationProvider',
  function ($routeProvider, $locationProvider) {
      $routeProvider
          .when('/', {
              templateUrl: 'Users',
              controller: 'UsersController'
          })
          .otherwise({
              redirectTo: '/'
          });
  }]);