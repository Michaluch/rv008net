﻿//Init DinningMenuItem controller
User.controller('DiningMenuItemsController', ['$scope', '$http', '$timeout', '$location', function ($scope, $http, $timeout, $location) {
    $scope.message = "";
    $scope.showHide = false;
    $scope.cartItem = {};
    $http.get("/User/DiningMenuItemsAjax?diningId=" + menuItemId)
        .success(function (data) {
            document.title = data.DinningName;
            $scope.menuItems1 = data.MenuItemsList;
            $scope.dishCategories = data.DishCategoriesList;
            if (!$scope.dishCategories.length > 0) {
                $scope.message = "Sorry.";
                $scope.showHide = true;
            }
        });

    //Get items of current category
    $scope.currentCategori = function (name) {
        var j = 0;
        $scope.menuItems = {};
        for (var i = 0; i < $scope.menuItems1.length; i++) {
            if ($scope.menuItems1[i].DishCategory == name) {
                $scope.menuItems[j] = $scope.menuItems1[i];
                j++;
            }
        }
    };

    //Send current item to UserLayoutController
    $scope.addToCart = function (index) {
        var messageText;
        if (sessionStorage.getItem("diningId") === null && $scope.cartItemsFromStorage.length != 0) {
            messageText = '<p><h4 class="notificationErrorTitle">Error</h4></p><p>Please, don\'t touch your session storage. Clean your cart, if you won\'t continue</p>';
            showNotification(messageText);
            return;
        }
        if (sessionStorage.getItem("diningId") === null || $scope.cartItemsFromStorage.length == 0) {
            sessionStorage.setItem('diningId', menuItemId);
            $scope.cartItem = $scope.menuItems[index];
            $scope.$emit('update_parent_controller', $scope.cartItem);
        } else {
            if (sessionStorage.getItem("diningId") == menuItemId) {
                $scope.cartItem = $scope.menuItems[index];
                $scope.$emit('update_parent_controller', $scope.cartItem);
            } else {
                messageText = '<p><h4 class="notificationErrorTitle">Sorry</h4></p><p>Your cart contain items from other dining</p>';
                showNotification(messageText);
            }
        }
    };

    //Accordion auto opener.
    $scope.count = 0;
    $scope.afterLoad = function () {
        if ($scope.dishCategories !== undefined) {
            $scope.count++;
        }
        if ($scope.count == 2) {
            $timeout(function () {
                $('#id0').trigger('click');
            }, 0);
        }
    };
}]);