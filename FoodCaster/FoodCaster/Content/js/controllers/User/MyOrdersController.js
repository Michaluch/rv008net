﻿//init MyOrders Controller
User.controller('MyOrdersController', ['$scope', '$http', '$timeout', '$location', function ($scope, $http, $timeout, $location) {
    document.title = "My orders";
    var container; //container for masonry items
    var msnry;     //object for working with masonry grid
    $scope.ordersLoaded = 0;
    $scope.allOrdersLoaded = false;
    //Get order list for current user after load page
    $http.get('/User' + $location.path() + "Ajax?ordersLoaded=" + $scope.ordersLoaded)
        .success(function (data) {
            $scope.OrdersList = data.OrdersList;
            $scope.allOrdersLoaded = data.AllOrdersLoaded;
            for (var i = 0; i < $scope.OrdersList.length; i++) {
                //user can't reopen order from not available dinning
                if (!$scope.OrdersList[i].DinningAvailable) {
                    $scope.OrdersList[i].PossibilityToReopen = false;
                } else {
                    //user can't reopen order, if all order items is not available
                    var unavailableItemsCount = 0;
                    for (var j = 0; j < $scope.OrdersList[i].OrderItems.length; j++) {
                        if (!$scope.OrdersList[i].OrderItems[j].ItemAvailable) {
                            unavailableItemsCount++;
                        }
                    }
                    if (unavailableItemsCount == $scope.OrdersList[i].OrderItems.length) {
                        $scope.OrdersList[i].PossibilityToReopen = false;
                    } else {
                        $scope.OrdersList[i].PossibilityToReopen = true;
                    }
                }
            }
            $scope.ordersLoaded = $scope.OrdersList.length;
            //masonry grid initialization
            $timeout(function () {
                container = document.querySelector('#container');
                msnry = new Masonry(container, {
                    itemSelector: '.myOrdersCardWrapper'
                });
                window.dispatchEvent(new Event('resize'));
            }, 0);
        });
    //After click on reload button
    $scope.reopen = function (index) {
        //Get new menu items pice from the server
        $http.post("/User/ReopenOrder", { orderId: $scope.OrdersList[index].Id })
            .success(function (data) {
                if (Boolean(data.success)) {
                    sessionStorage.setItem('diningId', $scope.OrdersList[index].DinningId);
                    //cleaning user cart
                    $scope.$emit('clear_cart');
                    //get desired address from base angular scope
                    if ($scope.addresses.GetAddressById($scope.OrdersList[index].AddressId) === null) {
                        //For not binded to user address
                        if (!$scope.OrdersList[index].AddressDeleted) {
                            var temp = {
                                UserId: $scope.userId,
                                Address: $scope.OrdersList[index].UserAddress,
                                isDefault: false,
                                Id: $scope.OrdersList[index].AddressId
                            };
                            $scope.addresses.push(temp);
                            $scope.$emit('set_current_address', temp.Id);
                            sessionStorage.setItem('currentAddressId', temp.Id);
                            sessionStorage.setItem('notBindedForUserAddress', JSON.stringify(temp));
                            //For deleted address
                        } else {
                            $scope.ReturnCurrentAddressToDefault();
                        }
                    } else {
                        $scope.$emit('set_current_address', $scope.OrdersList[index].AddressId);
                    }
                    //set new order items from the server
                    var orderItems = data.orderItems;
                    for (var i = 0; i < orderItems.length; i++) {
                        for (var j = 0; j < orderItems[i].Count; j++) {
                            $scope.$emit('update_parent_controller', data.orderItems[i]);
                        }
                    }
                } else {
                    var messageText = '<p><h4 class="notificationErrorTitle">Error</h4></p><p>' + data.message + '</p>';
                    showNotification(messageText);
                }
            });
    };
    //After click on abort button
    $scope.abort = function (index) {
        $http.post("/User/AbortOrder", { orderId: $scope.OrdersList[index].Id })
            .success(function (data) {
                var messageText;
                if (Boolean(data.success)) {
                    messageText = '<p><h4 class="notificationSuccessTitle">Aborted</h4></p><p>' + data.message + '</p>';
                    var scrollEnablingBefore = scrollEnabled();
                    var obj = $('#order' + index);
                    msnry.remove(obj);
                    msnry.layout();
                    $scope.OrdersList.splice(index, 1);
                    $timeout(function () {
                        var scrollEnablingAfter = scrollEnabled();
                        if (scrollEnablingBefore && !scrollEnablingAfter) {
                            loadOrders();
                        }
                    }, 0);
                } else {
                    messageText = '<p><h4 class="notificationErrorTitle">Error</h4></p><p>' + data.message + '</p>';
                }
                $scope.ordersLoaded--;
                showNotification(messageText);
            });
    };
    //Redirect to current MenuItems
    $scope.diningMenuitems = function (index) {
        menuItemId = $scope.OrdersList[index].DinningId;
        $location.path("DiningMenuItems");
    };

    $(document).unbind("scroll");
    $(document).on("scroll", function () {
        //loading next orders after scrolling to bottom  
        var scrollHeight = $(document).height();
        var scrollPosition = $(window).height() + $(window).scrollTop();
        if ((scrollHeight - scrollPosition) / scrollHeight === 0 &&
            $scope.ordersLoaded != 0 && !$scope.allOrdersLoaded) {
                loadOrders();
            }
    });
    $scope.openOrderItemsTab = function (index) {
        $('dd').has('#orderItemsTab' + index).addClass("active");
        $('dd').has('#trackInfoTab' + index).removeClass("active");
        $('#orderItems' + index).addClass("active");
        $('#trackInfo' + index).removeClass("active");
        msnry.layout();
    }
    $scope.openOrderTrackInfoTab = function (index) {
        $('dd').has('#orderItemsTab' + index).removeClass("active");
        $('dd').has('#trackInfoTab' + index).addClass("active");
        $('#orderItems' + index).removeClass("active");
        $('#trackInfo' + index).addClass("active");
        msnry.layout();
    }
    // if new order added on myorders page, it will be added to orders list
    $scope.$on('newOrderAdded', function (event, data) {
        $scope.OrdersList.unshift(data.newOrder);
        $scope.OrdersList[0].PossibilityToReopen = true;
        $timeout(function () {
            var obj = $('#order0');
            msnry.prepended(obj);
            msnry.reloadItems();
            msnry.layout();
        }, 0);
    });
    function loadOrders() {
        $http.get('/User' + $location.path() + "Ajax?ordersLoaded=" + $scope.ordersLoaded)
            .success(function (data) {
                for (var i = 0; i < data.OrdersList.length; i++) {
                    if (!data.OrdersList[i].DinningAvailable) {
                        data.OrdersList[i].PossibilityToReopen = false;
                    } else {
                        var unavailableItemsCount = 0;
                        for (var j = 0; j < data.OrdersList[i].OrderItems.length; j++) {
                            if (!data.OrdersList[i].OrderItems[j].ItemAvailable) {
                                unavailableItemsCount++;
                            }
                        }
                        if (unavailableItemsCount == data.OrdersList[i].OrderItems.length) {
                            data.OrdersList[i].PossibilityToReopen = false;
                        } else {
                            data.OrdersList[i].PossibilityToReopen = true;
                        }
                    }
                    $scope.OrdersList.push(data.OrdersList[i]);
                }
                $timeout(function () {
                    for (var i = 0; i < data.OrdersList.length; i++) {
                        var currentOrderId = $scope.OrdersList.length - 1;
                        var obj = $('#order' + currentOrderId);
                        msnry.appended(obj);
                        msnry.reloadItems();
                        msnry.layout();
                    }
                    $scope.ordersLoaded = $scope.OrdersList.length;
                    $scope.allOrdersLoaded = data.AllOrdersLoaded;
                }, 0);
            });
    }
    function scrollEnabled() {
        return $(document).height() > $(window).height();
    }
}]);