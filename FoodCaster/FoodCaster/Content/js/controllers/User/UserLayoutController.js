﻿//Init UserLayout Controller
User.controller('UserLayoutController', ['$scope', '$document', '$upload', '$http', '$location', '$timeout', function ($scope, $document, $upload, $http, $location, $timeout) {
    //----- Variables ----------------------------------------------   
    //true if click ok in modal window
    var ismodalok;
    //Saving user password ang hash for check
    $scope.userPass = {};
    //For changing new photo to old photo if something wrong
    $scope.OldPhoto = "/Content/img/Photo/noimage.jpg";
    //File uploading
    $scope.upload = [];
    //For cart moving
    $scope.activePosition = false;
    //Saving cart items in sessinon storage
    $scope.cartItemsFromStorage = [];
    //----- Variables end ------------------------------------------  

    //----- User Profile -------------------------------------------  
    //Show modal window for edit user data
    $scope.ShowEditUser = function () {
        $scope.userPass = {};
        $('#Action').html("Change user data");
        $("div.error").attr("class", "small-5 small-pull-3 columns");
        $("label.error").attr("class", "right inline");
        $('#modalEditUser').foundation('reveal', 'open');
        $scope.OldPhoto = $scope.currentUser.Photo;
        ismodalok = false;
        document.getElementById("uploadingPhoto").value = "";
    };
    //Remove User PhotoLink from cancelled modal window 
    $('#modalEditUser').on('closed', function () {
        if ($scope.currentUser.Photo != "/Content/img/Photo/noimage.jpg") {
            if ($scope.currentUser.Photo != $scope.OldPhoto) {
                if (ismodalok == false) {
                    $.post("/User/DeletePhotoLink", {
                        FileName: $scope.currentUser.Photo
                    }).done(
                $scope.currentUser.Photo = $scope.OldPhoto);
                    $scope.$apply();
                }
            }
        }
    });
    //Operations with server after click ok in modal window
    $scope.ModalOk = function () {
        if ($("div.error").length == 0) {
            if ($scope.userPass.confirmPass == $scope.userPass.Pass) {
                if ($scope.userPass.Pass == null) $scope.userPass.Pass = "";
                if ($scope.userPass.Pass.length >= 8 || $scope.userPass.Pass == "") {
                    var ShaString = CryptoJS.SHA512($scope.userPass.Pass).toString(CryptoJS.enc.Hex);
                    if ($scope.userPass.Pass == "") ShaString = "";
                    $.post("/User/EditUserData",
                        {
                            "Id": $scope.currentUser.Id,
                            "Photo": $scope.currentUser.Photo,
                            "Email": $scope.currentUser.Email,
                            "UserName": $scope.currentUser.UserName,
                            "Password": ShaString,
                            "Role":$scope.currentUser.Role
                        })
                        .done(function (data) {
                            var messageText = "";
                            if (Boolean(data.success)) {
                                ismodalok = true;
                                $('#modalEditUser').foundation('reveal', 'close');
                                document.getElementById("uploadingPhoto").value = "";
                                messageText = '<p><h4 class="notificationSuccessTitle">Changed</h4></p><p>' + data.message + '</p>';
                            } else {
                                messageText = '<p><h4 class="notificationErrorTitle">Error</h4></p><p>' + data.message + '</p>';
                            };
                            showNotification(messageText);
                            $scope.$apply();
                        });
                };
            };
        } else {
            messageText = '<p><h4 class="notificationErrorTitle">Error</h4></p><p>Data in form is not valid</p>';
            showNotification(messageText);
        };
    };
    //Angular upload module for user photo
    $scope.onFileSelect = function ($files) {       
        for (var i = 0; i < $files.length; i++) {
            var $file = $files[i];
            (function (index) {
                $scope.upload[index] = $upload.upload({
                    url: "/User/AddPhotoLink", 
                    method: "POST",
                    data: { uploadedFile: $file },
                    file: $file
                }).success(function (data, status, headers, config) {
                    if (data.success) {
                        $scope.OldPhoto = $scope.currentUser.Photo;
                        $scope.currentUser.Photo = data.Photourl;

                    }
                }).error(function (data, status, headers, config) {
                    // file failed to upload
                    showNotification('Error occured during upload');
                });
            })(i);
        }
    };
    //Geting data about user from the server
    $http.get("/User/ViewUserAjax").
        success(function (data) {
            $scope.currentUser = data;
        })
        .error(function (status) {
        });
    //----- User Profile end ---------------------------------------

    //----- Cart --------------------------------------------------- 
    //Change cart items count from the shopping cart
    $scope.MinusCount = function(count, id) {
        count--;
        if (count < 1 || count == "") {
            count = 1;
        }
        $scope.ChangeCount(count, id);
    };
    $scope.PlusCount = function (count, id) {
        count++;
        $scope.ChangeCount(count, id);
    };
    $scope.ChangeCount = function(count, id) {
        $scope.NullCounter = 0;
        if (count == "") {
            $scope.InputCount = "";
        } else if (count > 1000) {
            $scope.InputCount = 1000;
        } else {
            $scope.InputCount = count;
        }
        //Set focus for the current input
        $timeout(function() {
            $('.item-quality input#count' + id).focus();
        }, 0);
        $scope.Item = [];
        for (var p = 0; p < sessionStorage.length; p++) {
            if (JSON.parse(sessionStorage.getItem(sessionStorage.key(p))).Id == id) {
                $scope.Item = JSON.parse(sessionStorage.getItem(sessionStorage.key(p)));
            }
        }
        sessionStorage.setItem('cartItem' + $scope.Item.Id, JSON.stringify({
            Id: $scope.Item.Id,
            Name: $scope.Item.Name,
            Price: $scope.Item.Price,
            Count: $scope.InputCount,
            TotalPrice: $scope.Item.Price * $scope.InputCount
        }));
        for (var i = 0; i < sessionStorage.length; i++) {
            if (JSON.parse(sessionStorage.getItem(sessionStorage.key(i))).Count == "") {
                $scope.NullCounter++;
            }
        }
        //Button disabled if one or more fields are empty
        if ($scope.NullCounter !== 0) {
            $('#sendData').addClass('disabled').attr("disabled", true).css("cursor", "not-allowed");
        } else {
            $('#sendData').removeClass('disabled').attr("disabled", false).css("cursor", "pointer");
        }
        $scope.GetItems();
    };
    $scope.$on("clear_cart", function(event) {
        for (i = 0; i < $scope.cartItemsFromStorage.length; ++i) {
            $scope.DeleteItemFromCart($scope.cartItemsFromStorage[i].Id);
            --i;
        }
    });
    //Set current address in cart
    $scope.$on("set_current_address", function (event, Id) {
        var tempAddr = $scope.addresses.filter(function (addr) {
            return addr.Id == Id;
        });
        $scope.currentAddress = tempAddr[0];
        sessionStorage.setItem('currentAddressId', $scope.currentAddress.Id);
    });
    //Get data from DiningMenuItemsController
    $scope.$on("update_parent_controller", function (event, cartItem) {
        var messageText;
        $scope.cartItem = cartItem;
        if (window.innerWidth < 500) {
            $scope.activePosition = false;
            messageText = cartItem.Name + " was added.";
            showNotification(messageText);
        } else {
            $scope.activePosition = true;
        }
        sessionStorage.setItem('activePosition', JSON.stringify($scope.activePosition));
        $scope.ItmCount = 1;
        for (var k = 0; k < sessionStorage.length; k++) {
            if (sessionStorage.key(k) == 'cartItem' + $scope.cartItem.Id) {
                $scope.ItmCount = JSON.parse(sessionStorage.getItem(sessionStorage.key(k))).Count + 1;
            }
        }
        if ($scope.ItmCount > 1000) {
            $scope.ItmCount = 1000;
            }
        sessionStorage.setItem('cartItem' + $scope.cartItem.Id, JSON.stringify({
            Id: $scope.cartItem.Id,
            Name: $scope.cartItem.Name,
            Price: parseFloat($scope.cartItem.Price.replace(/,/,'.')),
            Count: $scope.ItmCount,
            TotalPrice: parseFloat($scope.cartItem.Price.replace(/,/, '.')) * $scope.ItmCount
        }));
        $scope.GetItems();
    });
    //Get items from the session storage
    $scope.GetItems = function () {
        $scope.cartItemsFromStorage = [];
        for (var p = 0; p < sessionStorage.length; p++) {
            if (sessionStorage.key(p).indexOf('cartItem') > -1) {
                $scope.cartItemsFromStorage.push(JSON.parse(sessionStorage.getItem(sessionStorage.key(p))));
            }
        }
        for (var i = 0; i < sessionStorage.length; i++) {
            if (sessionStorage.key(i).indexOf('totalPrc') > -1) {
                $scope.totalPrc = JSON.parse(sessionStorage.getItem(sessionStorage.key(i)));
            }
        }
        $scope.totalPrc = 0;
        for (var j = 0; j < $scope.cartItemsFromStorage.length; j++) {
            $scope.totalPrc += $scope.cartItemsFromStorage[j].TotalPrice;
        }
        sessionStorage.setItem('totalPrc', JSON.stringify($scope.totalPrc));
    };
    //Set cart active position
    $scope.activePosition = JSON.parse(sessionStorage.getItem('activePosition'));
    //Show-hide cart
    $scope.toggleDetail = function () {
        $scope.activePosition = $scope.activePosition == true ? false : true;
        sessionStorage.setItem('activePosition', JSON.stringify($scope.activePosition));
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };
    //Close cart
    $scope.closeCart = function () {
        $scope.activePosition = false;
        sessionStorage.setItem('activePosition', JSON.stringify($scope.activePosition));
    };
    //Method for remove items from the shopping cart
    $scope.DeleteItemFromCart = function (id) {
        for (var i = 0; i < sessionStorage.length; i++) {
            if (sessionStorage.key(i) != 'totalPrc' && JSON.parse(sessionStorage.getItem(sessionStorage.key(i))).Id == id) {
                sessionStorage.removeItem(sessionStorage.key(i));
                $scope.cartItemsFromStorage.splice(i, 1);
            }
        }
        $scope.GetItems();
    };
    //Add new order from the cart
    $scope.SendData = function () {
        var returnNewOrderData = (window.location.toString().indexOf("MyOrders") > -1);
        var ItemsList = [];
        for (i = 0; i < $scope.cartItemsFromStorage.length; i++) {
            var item = {
                "Id": $scope.cartItemsFromStorage[i].Id,
                "Count": $scope.cartItemsFromStorage[i].Count
            }
            ItemsList.push(item);

        }
        $scope.closeCart();
        sessionStorage.clear();
        $scope.cartItemsFromStorage = [];
        $scope.totalPrc = 0;
        $.post("/User/AddNewOrder",
            {
                "addressId": $scope.currentAddress.Id,
                "returnNewOrderData" : returnNewOrderData,
                "itemsIdList": ItemsList
            })
            .done(function (data) {
                if (Boolean(data.success)) {
                    messageText = '<p><h4 class="notificationSuccessTitle">Successfully</h4></p><p>' + data.message + '</p>';
                    if (returnNewOrderData) {
                        $scope.$broadcast('newOrderAdded', {
                            newOrder: data.newOrder
                        });
                    }
                } else {
                    messageText = '<p><h4 class="notificationErrorTitle">Error</h4></p><p>' + data.message + '</p>';
                };
                showNotification(messageText);
                $scope.$apply();
            });
    };
    $scope.GetItems();
    //----- Cart end------------------------------------------------ 

    //----- Addresses -----
    //Function for load addresses from the server
    function loadAddresses() {
        $http.post("/User/AddressesAjax")
            .success(function (data) {
                $scope.userId = data.UserId;
                $scope.addresses = data.addresses;
                $scope.addresses.GetAddressById = function (id) {
                    for (var i = 0; i < $scope.addresses.length; i++) {
                        if ($scope.addresses[i].Id == id) {
                            return $scope.addresses[i];
                        }
                    }
                    return null;
                }
                if (sessionStorage.getItem("currentAddressId") === null) {
                    $scope.ReturnCurrentAddressToDefault();
                } else {
                    if ($scope.addresses.GetAddressById(sessionStorage.getItem("currentAddressId")) === null) {
                        $scope.addresses.push(JSON.parse(sessionStorage.getItem('notBindedForUserAddress')));
                        $scope.currentAddress = $scope.addresses[$scope.addresses.length - 1];
                    } else {
                        $scope.currentAddress = $scope.addresses.GetAddressById(sessionStorage.getItem("currentAddressId"));
                    }

                }
            });
    };
    $(document).on('change', '#addressesSelect', function () {
        sessionStorage.setItem('currentAddressId', $scope.currentAddress.Id);
    });
    //Show modal window for add new address from the cart
    $scope.ShowModalAdd = function () {
        $('#addressOnCart').val("");
        $('#isDefaultOnCart').prop('checked', false);
        $('#bindToUserOnCart').prop('checked', true);
        $('#isDefaultOnCart').prop('disabled', false);
        $("div.error").attr("class", "small-7 small-pull-1 columns");
        $("label.error").attr("class", "small-4 columns");
        $('#modalAddressOnCart').foundation('reveal', 'open');
    };
    $('#bindToUserOnCart').change(function () {
        if ($('#bindToUserOnCart').prop('checked')) {
            $('#isDefaultOnCart').prop('disabled', false);
        } else {
            $('#isDefaultOnCart').prop('checked', false);
            $('#isDefaultOnCart').prop('disabled', true);
        };
    });
    //Saving address from the cart
    $scope.SaveAddressFromCart = function () {
        var isDefaultAddress;
        if ($('#isDefaultOnCart').prop('disabled')) {
            isDefaultAddress = false;
        } else {
            isDefaultAddress = $('#isDefaultOnCart').prop('checked');
        }
        var userId = null;
        if ($('#bindToUserOnCart').prop('checked'))
            userId = $scope.userId
        $.post("/User/AddAddress",
            {
                UserId:  userId,
                Address: $('#addressOnCart').val(),
                IsDefault: isDefaultAddress,
            })
            .done(function (data) {
                if (Boolean(data.success)) {
                    if ($('#isDefaultOnCart').prop('checked')) {
                        for (var i = 0; i < $scope.addresses.length; i++) {
                            $scope.addresses[i].IsDefault = false;
                        }
                    }
                    $scope.currentAddress = data.newAddress;
                    $scope.addresses.push($scope.currentAddress);
                    var messageText = '<p><h4 class="notificationSuccessTitle">Successfully</h4></p><p>' + data.message + '</p>';
                    showNotification(messageText);
                    
                    sessionStorage.setItem('currentAddressId', $scope.currentAddress.Id);
                    if (!$('#bindToUserOnCart').prop('checked')) {
                        sessionStorage.setItem('notBindedForUserAddress', JSON.stringify($scope.currentAddress));
                    };
                    $scope.$apply();
                    $('#modalAddressOnCart').foundation('reveal', 'close');
                } else {
                    messageText = '<p><h4 class="notificationErrorTitle">Error</h4></p><p>' + data.message + '</p>';
                    showNotification(messageText);
                };
            });
    };
    $scope.ReturnCurrentAddressToDefault = function () {
        for (var i = 0; i < $scope.addresses.length; i++) {
            if ($scope.addresses[i].IsDefault) {
                $scope.currentAddress = $scope.addresses[i];
                sessionStorage.setItem('currentAddressId', $scope.currentAddress.Id);
            }
        }
    };
    loadAddresses();
    //----- Addresses end ------------------------------------------

    $scope.SignOut = function () {
        sessionStorage.clear();
        location.href = '/Authorization/LogIn';
    };
}]);