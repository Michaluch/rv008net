﻿//init Addresses Controller
User.controller('AddressesController', ['$scope', '$http', '$location', function ($scope, $http, $location) {
    document.title = "My addresses";
    $scope.defaultStat = [true, false];
    $scope.modal = {};
    //Show modal window for add new address
    $scope.ShowModalAdd = function () {
        $scope.modal = {};
        $scope.modal.type = "Add";
        $('#isDefault').prop('checked', false);
        $("div.error").attr("class", "small-7 small-pull-1 columns");
        $("label.error").attr("class", "small-4 columns");
        $('#ModalTitle').html("Add new address");
        $('#Action').html("Add");
        $('#modalAddress').foundation('reveal', 'open');
    };
    //Show modal window for edit address
    $scope.ShowModalEdit = function (index) {
        $scope.modal = {};
        $scope.modal.address = $scope.addresses[index].Address;
        $('#isDefault').prop('checked', $scope.addresses[index].IsDefault);
        $scope.modal.type = "Edit";
        $scope.modal.itemId = index;
        $("div.error").attr("class", "small-7 small-pull-1 columns");
        $("label.error").attr("class", "right inline");
        $('#ModalTitle').html("Edit address");
        $('#Action').html("Save");
        $('#modalAddress').foundation('reveal', 'open');
    };
    //After click on ok button in modal window
    $scope.ModalOk = function () {
        var messageText = "";
        if ($scope.modal.type == "Add") {
            //If we want Add Address
            $.post("/User/AddAddress",
                {
                    UserId: $scope.userId,
                    Address: $scope.modal.address,
                    IsDefault: $('#isDefault').prop('checked'),
                })
            .done(function (data) {
                if (Boolean(data.success)) {
                    if ($('#isDefault').prop('checked') == true) {
                        for (var i = 0; i < $scope.addresses.length; i++) {
                            $scope.addresses[i].IsDefault = false;
                        }
                    }
                    $scope.addresses.push(data.newAddress);
                    messageText = '<p><h4 class="notificationSuccessTitle">Successfully</h4></p><p>' + data.message + '</p>';
                    showNotification(messageText);
                    $('#modalAddress').foundation('reveal', 'close');
                } else {
                    messageText = '<p><h4 class="notificationErrorTitle">Error</h4></p><p>' + data.message + '</p>';
                    showNotification(messageText);
                };
                $scope.$apply();
            });
        } else {
            //If we want Edit Address
            var messageText = "";
            $.post("/User/EditAddress",
               {
                   Id: $scope.addresses[$scope.modal.itemId].Id,
                   UserId: $scope.userId,
                   Address: $scope.modal.address,
                   IsDefault: $('#isDefault').prop('checked')
               })
            .done(function (data) {
                if (Boolean(data.success)) {
                    if ($('#isDefault').prop('checked') == true) {
                        for (i = 0; i < $scope.addresses.length; i++) {
                            $scope.addresses[i].IsDefault = false;
                        }
                    }
                    $scope.addresses[$scope.modal.itemId].Address = $scope.modal.address;
                    $scope.addresses[$scope.modal.itemId].IsDefault = $('#isDefault').prop('checked');
                    messageText = '<p><h4 class="notificationSuccessTitle">Successfully</h4></p><p>Address data was changed</p>';
                    showNotification(messageText);
                    $('#modalAddress').foundation('reveal', 'close');
                    $scope.$apply();
                } else {
                    messageText = '<p><h4 class="notificationErrorTitle">Error</h4></p><p>' + data.message + '</p>';
                    showNotification(messageText);
                };
            });
        };

    };
    //Function for delete address
    $scope.Delete = function (index) {
        $.post("/User/DeleteAddress",
                {
                    Id:$scope.addresses[index].Id,
                    UserId: $scope.userId,
                    Address: $scope.addresses[index].Address
                })
        .done(function (data) {
            if (Boolean(data.success)) {
                if ($scope.addresses[index].Id == $scope.currentAddress.Id) {
                    $scope.ReturnCurrentAddressToDefault();
                }
                $scope.addresses.splice(index, 1);
                messageText = '<p><h4 class="notificationSuccessTitle">Successfully</h4></p><p>' + data.message + '</p>';
                showNotification(messageText);
            } else {
                messageText = '<p><h4 class="notificationErrorTitle">Error</h4></p><p>' + data.message + '</p>';
                showNotification(messageText);
            };
            $scope.$apply();
        });
    }
}]);