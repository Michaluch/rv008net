var User = angular.module('User', ['angularFileUpload', 'User.directives']);
//global var for save menu item id
var menuItemId;
//User route configuration
User.config(['$routeProvider','$locationProvider',
  function($routeProvider, $locationProvider) {
      $routeProvider
          .when('/', {
              templateUrl: 'MyFoodCaster',
              controller: 'MyFoodCasterCtrl'
          })
          .when('/MyOrders', {
              templateUrl: 'MyOrders',
              controller: 'MyOrdersController'
          })
          .when('/CompletedOrders', {
              templateUrl: 'CompletedOrders',
              controller: 'MyOrdersController',
          })
          .when('/Addresses', {
              templateUrl: 'Addresses',
              controller: 'AddressesController'
          })
          .when('/DiningMenuItems', {
              templateUrl: 'DiningMenuItems',
              controller: 'DiningMenuItemsController'
          })
          .when('/ViewUser', {
              templateUrl: 'ViewUser'
          })
          .otherwise({
              redirectTo: '/'
          });
  }]);