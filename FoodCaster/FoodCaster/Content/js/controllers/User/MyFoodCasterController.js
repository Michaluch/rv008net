﻿//Init MyFoodCaster controller
User.controller('MyFoodCasterCtrl', function ($scope, $http, $location) {
    document.title = "My food caster";
    $http.get('/User/MyFoodCasterAjax')
        .success(function (data) {
            $scope.restaurants = data.DinningList;
            $scope.categories = data.CategoryList;
            $scope.filteredrest = data.DinningList;
        });
    $scope.GetByCategory = function (item) {
        $scope.filteredrest = {};
        var j = 0;
        for (i = 0; i < $scope.restaurants.length; i++) {
            if ($scope.restaurants[i].Category == item) {
                $scope.filteredrest[j] = $scope.restaurants[i];
                j++;
            }
        }
    };
    $scope.ShowAll = function () {
        $scope.filteredrest = $scope.restaurants;
    };
    //Redirect to show menu of current restaurant
    $scope.Menu = function (index) {
        var rest = $scope.filteredrest[index];
        menuItemId = rest.Id;
        $location.path("DiningMenuItems");
    };
});