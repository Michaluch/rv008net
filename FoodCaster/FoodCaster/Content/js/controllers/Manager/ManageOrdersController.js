﻿ManagerModule.controller('ManageOrdersController', function ($scope, $http, $location) {
    document.title = "Manage orders";
    //Array for saving objects in one page
    $scope.items = {};
    //Array for saving all objects
    $scope.orders = {};
    //Options for set items count in one page
    $scope.countVariants = [10, 20, 40, 60, 100];
    //Set default (how many pages)
    $scope.pageCount = 0;
    //Set count of items in one page
    $scope.countItems = 10;
    //Set default current page
    $scope.currentPage = 0;
    //Use css class 'active' for 'Confirmed' filter
    $scope.tog = 1;
    //Max page link for view
    $scope.viewLinks = 5;
    //Load data from server
    $scope.loadOrders = function (status) {
        $scope.currentStatus = status;
        var result = $http.post("/Manager/ManageOrdersAjax", { status: $scope.currentStatus });
        result.success(function (data) {
            $scope.orders = data.orderList.reverse();
            $scope.RebuildPages();
        });
    };
    //Load data for start page
    $scope.loadOrders('Confirmed');
    //Rebuilding paginations and all data in items
    $scope.RebuildPages = function () {
        $scope.countItems = 10;
        if ($scope.countItems > $scope.orders.length) {
            $scope.countItems = $scope.orders.length;
        }
        for (i = 0; i < $scope.countItems; i++) {
            $scope.items[i] = $scope.orders[i];
        }
        $scope.lastPage = Math.ceil(($scope.orders.length) / $scope.countItems) - 1;
        $scope.setPage($scope.currentPage);
    };
    //Show current page + next page and prev page
    $scope.setPage = function (n) {
        $scope.lastPage = Math.ceil(($scope.orders.length) / $scope.countItems) - 1;
        if (n >= 0) {
            if (n > $scope.lastPage) { $scope.currentPage = $scope.lastPage; }
            else { $scope.currentPage = n; }
            $scope.items = {};
            for (i = 0; i < $scope.countItems; i++) {
                if ($scope.orders[i + $scope.currentPage * $scope.countItems] == null) continue;
                $scope.items[i] = $scope.orders[i + $scope.currentPage * $scope.countItems];
            }
        };
    };
    $scope.prevPage = function () {
        $scope.currentPage--;
        $scope.setPage($scope.currentPage);

    };
    $scope.nextPage = function () {
        $scope.currentPage++;
        $scope.setPage($scope.currentPage);
    };
    //Get pagination links int array (1,2,3..)
    $scope.range = function () {
        var rangeSize = $scope.viewLinks;
        var ret = [];
        var start = 0;
        if ($scope.currentPage > $scope.viewLinks - 2) {
            start = $scope.currentPage;
        }
        for (var i = start; i < start + rangeSize; i++) {
            if (i > ($scope.orders.length - 1) / $scope.countItems) continue;
            ret.push(i);
        }
        $scope.pageCount = ret.length - 1;
        return ret;
    };
    //Show/Hide and load MenuItems to the session storage after click on current order item
    $scope.toggleDetail = function ($index, ordrId) {

        for (var i = 0; i < $scope.orders.length; i++) {
            if ($scope.orders[i].Id == ordrId) {
                $scope.ord_id = ordrId;
            }
        }

        for (var j = 0; j < sessionStorage.length; j++) {
            if (sessionStorage.key(j) == 'key' + [$scope.ord_id]) {
                $scope.key = sessionStorage.key(j);
            }
        }

        if ($scope.activePosition != $index) {
            if ($scope.key == 'key' + [$scope.ord_id]) {
                $scope.itemList = JSON.parse(sessionStorage.getItem('key' + [$scope.ord_id]));
            } else {
                var res = $http.post("/Manager/OrderItems", { orderId: $scope.ord_id });
                res.success(function (data) {
                    $scope.itemList = data.itemsList;
                    sessionStorage.setItem('key' + [$scope.ord_id], JSON.stringify(data.itemsList));
                });
            }
        }
        $scope.activePosition = $scope.activePosition == $index ? -1 : $index;
    };
    //Show action link if status = Confirmed
    $scope.ShowValue = function (status) {
        if (status == 'Confirmed') {
            return true;
        } else {
            return false;
        }
    };
    //Send post in server to change current order status = Completed
    $scope.CompleteOrder = function (orderId) {
        var messageText = "";
        var itemid;
        for (var i = 0; i < $scope.orders.length; i++) {
            if ($scope.orders[i].Id == orderId) {
                $scope.ord_id = orderId;
                itemid = i;
            } else {
                messageText = '<p><h4 class="notificationErrorTitle">Error</h4></p>';
            }
        }
        $http.post("/Manager/CompleteOrder", { OrderId: $scope.ord_id })
            .success(function (data) {
                if (Boolean(data.success)) {

                    for (var j = 0; j < $scope.orders.length; j++) {
                        if ($scope.orders[j].Id == $scope.ord_id) {
                            $scope.orders[j].Status = "Completed";
                            $scope.orders.splice(itemid, 1);
                            $scope.RebuildPages();
                        } else {
                            messageText = '<p><h4 class="notificationErrorTitle">Error</h4></p>';
                        }
                    }
                    messageText = '<p><h4 class="notificationSuccessTitle">Completed</h4></p><p>' + data.message + '</p>';
                } else {
                    messageText = '<p><h4 class="notificationErrorTitle">Error</h4></p><p>' + data.message + '</p>';
                }
                showNotification(messageText);
                $scope.includeStatus();
                if (!$scope.$$phase) {
                    $scope.$apply();
                }

            });
    };
    $scope.statusIncludes = [];
    //Change order status from view
    $scope.ChangeStatus = function (Status) {
        $scope.currentStatus = Status;
        $scope.loadOrders(Status);
    };
});