﻿ManagerModule.controller('MenuItemsController', function ($scope, $http, $location) {
    var getParams = getUrlVar();
    $http.get("/Manager/MenuItemsAjax?diningId=" + ManagerModule.menuItemId)
        .success(function (data) {
            document.title = data.DinningName;
            $scope.diningId = data.DiningId;
            $scope.menuItems = data.MenuItemsList;
            $scope.dishCategories = data.DishCategoriesList;
            $scope.menuItemStatuses = data.MenuItemStatusesList;
        });

    $scope.Remove = function (index) {
        $http.post("/Manager/MenuItemDelete", { id: $scope.menuItems[index].Id })
            .success(function (data) {
                var messageText;
                if (Boolean(data.success)) {
                    messageText = '<p><h4 class="notificationSuccessTitle">Deleted</h4></p><p>' + data.message + '</p>';
                    $scope.menuItems.splice(index, 1);
                } else {
                    messageText = '<p><h4 class="notificationErrorTitle">Error</h4></p><p>' + data.message + '</p>';
                }
                showNotification(messageText);
                //$scope.$apply();
            });
    }

    $scope.Add = function () {
        $('#menuItemName').val("");
        $('#menuItemPrice').val("");
        $('#menuItemDescription').val("");

        var dishSelect = document.getElementById('menuItemDishCategory');
        while (dishSelect.firstChild) {
            dishSelect.removeChild(dishSelect.firstChild);
        }
        for (var i = 0; i < $scope.dishCategories.length; i++) {
            var opt = document.createElement('option');
            opt.innerHTML = $scope.dishCategories[i].Name;
            dishSelect.appendChild(opt);
        }

        var statusSelect = document.getElementById('menuItemStatus');
        while (statusSelect.firstChild) {
            statusSelect.removeChild(statusSelect.firstChild);
        }
        for (var i = 0; i < $scope.menuItemStatuses.length; i++) {
            var opt = document.createElement('option');
            opt.innerHTML = $scope.menuItemStatuses[i].Name;
            if ($scope.menuItemStatuses[i].Name == "Aviable")
                opt.selected = true;
            statusSelect.appendChild(opt);
        }

        $('#ModalMenuItemTitle').text('Add menu item');
        $("div.error").attr("class", "small-7 small-pull-1 columns");
        $("label.error").attr("class", "right inline");
        $('#modalMenuItem').foundation('reveal', 'open');

        $("#modalSave").unbind();
        $('#modalSave').click(function () {
            if ($('#menuItemName').val() == "") {
                $('#menuItemName').focus();
                return;
            }
            if ($('#menuItemPrice').val() == "") {
                $('#menuItemPrice').focus();
                return;
            }
            if ($('#menuItemDescription').val() == "") {
                $('#menuItemDescription').focus();
                return;
            }
            if ($("div.error").length == 0) {
                $('#modalMenuItem').foundation('reveal', 'close');
                $http.post("/Manager/MenuItemAdd", {
                    Name:$('#menuItemName').val(),
                    Price: $('#menuItemPrice').val(),
                    Description: $('#menuItemDescription').val(),
                    DishCategoryId: GetIdByName($scope.dishCategories, $('#menuItemDishCategory').val()),
                    DinningId: $scope.diningId,
                    CurrentStatusId:GetIdByName($scope.menuItemStatuses, $('#menuItemStatus').val())
                })
                    .success(function (data) {
                        var messageText;
                        if (Boolean(data.success)) {
                            var price = $('#menuItemPrice').val();
                            if (price.indexOf(',') > -1)
                                price = price.replace(',', '.');
                            var tmp = {
                                Name: $('#menuItemName').val(),
                                Price: price,
                                Description: $('#menuItemDescription').val(),
                                DishCategory: $('#menuItemDishCategory').val(),
                                CurrentMenuItemsStatus: $('#menuItemStatus').val()
                            };
                            messageText = '<p><h4 class="notificationSuccessTitle">Added</h4></p><p>' + data.message + '</p>';
                            tmp.Id = data.id;
                            $scope.menuItems.push(tmp);
                        } else {
                            messageText = '<p><h4 class="notificationErrorTitle">Error</h4></p><p>' + data.message + '</p>';
                        }
                        showNotification(messageText);
                        //$scope.$apply();
                    });
            }
        });
    }

    $scope.Edit = function (index) {
        $('#menuItemName').val($scope.menuItems[index].Name);
        $('#menuItemPrice').val($scope.menuItems[index].Price);
        $('#menuItemDescription').val($scope.menuItems[index].Description);

        var dishSelect = document.getElementById('menuItemDishCategory');
        while (dishSelect.firstChild) {
            dishSelect.removeChild(dishSelect.firstChild);
        }
        for (var i = 0; i < $scope.dishCategories.length; i++) {
            var opt = document.createElement('option');
            opt.innerHTML = $scope.dishCategories[i].Name;
            if ($scope.dishCategories[i].Name == $scope.menuItems[index].DishCategory)
                opt.selected = true;
            dishSelect.appendChild(opt);
        }

        var statusSelect = document.getElementById('menuItemStatus');
        while (statusSelect.firstChild) {
            statusSelect.removeChild(statusSelect.firstChild);
        }
        for (var i = 0; i < $scope.menuItemStatuses.length; i++) {
            var opt = document.createElement('option');
            opt.innerHTML = $scope.menuItemStatuses[i].Name;
            if ($scope.menuItemStatuses[i].Name == $scope.menuItems[index].CurrentMenuItemsStatus)
                opt.selected = true;
            statusSelect.appendChild(opt);
        }

        $('#ModalMenuItemTitle').text('Change menu item');
        $("div.error").attr("class", "small-7 small-pull-1 columns");
        $("label.error").attr("class", "right inline");
        $('#modalMenuItem').foundation('reveal', 'open');

        $("#modalSave").unbind();
        $('#modalSave').click(function () {
            if ($('#menuItemName').val() == "") {
                $('#menuItemName').focus();
                return;
            }
            if ($('#menuItemPrice').val() == "") {
                $('#menuItemPrice').focus();
                return;
            }
            if ($('#menuItemDescription').val() == "") {
                $('#menuItemDescription').focus();
                return;
            }
            if ($("div.error").length == 0) {
                $('#modalMenuItem').foundation('reveal', 'close');
                $http.post("/Manager/MenuItemEdit", {
                    Id:$scope.menuItems[index].Id,
                    Name:$('#menuItemName').val(),
                    Price: $('#menuItemPrice').val(),
                    Description: $('#menuItemDescription').val(),
                    DishCategoryId: GetIdByName($scope.dishCategories, $('#menuItemDishCategory').val()),
                    DinningId: $scope.diningId,
                    CurrentStatusId:GetIdByName($scope.menuItemStatuses, $('#menuItemStatus').val())
                })
                    .success(function (data) {
                        var messageText;
                        if (Boolean(data.success)) {
                            messageText = '<p><h4 class="notificationSuccessTitle">Changed</h4></p><p>' + data.message + '</p>';
                            var price = $('#menuItemPrice').val();
                            if (price.indexOf(',') > -1)
                                price = price.replace(',', '.');
                            $scope.menuItems[index].Name = $('#menuItemName').val();
                            $scope.menuItems[index].Price = price;
                            $scope.menuItems[index].Description = $('#menuItemDescription').val();
                            $scope.menuItems[index].DishCategory = $('#menuItemDishCategory').val();
                            $scope.menuItems[index].CurrentMenuItemsStatus = $('#menuItemStatus').val();
                        } else {
                            messageText = '<p><h4 class="notificationErrorTitle">Error</h4></p><p>' + data.message + '</p>';
                        }
                        showNotification(messageText);
                        //$scope.$apply();
                    });
            }
        });
    }

    function GetIdByName(arr, name) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i].Name == name) return arr[i].Id;
        }
        return null;
    }

    function getUrlVar() {
        var urlVar = window.location.search;
        var arrayVar = [];
        var valueAndKey = [];
        var resultArray = [];
        arrayVar = (urlVar.substr(1)).split('&');
        if (arrayVar[0] == "") return false;
        for (i = 0; i < arrayVar.length; i++) {
            valueAndKey = arrayVar[i].split('=');
            resultArray[valueAndKey[0]] = valueAndKey[1];
        }
        return resultArray;
    }
});