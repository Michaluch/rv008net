﻿ManagerModule.controller('StatisticsController', function ($scope, $http, $timeout) {
    //Full restaurants list
    $scope.chartDataRestaurant = [];
    //Restaurants list with short names
    $scope.chartSubDataRestaurant = [];
    //Restaurants list included in the top
    $scope.chartDataRestaurantTop = [];
    //Restaurants list that are not included in the top
    $scope.chartDataRestaurantOther = [];
    //Set the number of top restaurants
    $scope.topCountRestaurant = 5;
    //Set the number of top dishes
    $scope.topCountMenu = 5;
    //Segment color
    var color;
    //Set labels length
    var chartTitleLength = 14;
    var chartMaxTitleLength = 16;
    $http.get("/Manager/StatisticsAjax")
        .success(function (data) {
        $scope.diningOrdersCountList = data;
        for (var i = 0; i < $scope.diningOrdersCountList.length; i++) {
            color = String(randomHexColor());
            $scope.chartDataRestaurant.push({
                "label": String($scope.diningOrdersCountList[i].RestaurantName),
                "value": $scope.diningOrdersCountList[i].Count,
                "color": color
            });
        };
        //Sort restaurants(descending)
        $scope.chartDataRestaurant.sort(function(obj1, obj2) {
            return obj2.value - obj1.value;
        });
        $scope.chartSubDataRestaurant = $scope.GetChartDataWithSubTitle($scope.chartDataRestaurant, chartTitleLength, chartMaxTitleLength);
        //Get Top restaurants
        for (var k = 0; k < Math.min($scope.topCountRestaurant, $scope.chartDataRestaurant.length); k++) {
            $scope.chartDataRestaurantTop[k] = $scope.chartDataRestaurant[k];
        };
        //Get restaurants that are not included in the top
        for (var l = $scope.topCountRestaurant; l < $scope.chartDataRestaurant.length; l++) {
            $scope.chartDataRestaurantOther.push($scope.chartDataRestaurant[l]);
        };
        $scope.othersLengthRestaurant = $scope.chartDataRestaurantOther.length;
        //Draw restaurants chart
        var pie = new d3pie("pieChart1", SetChartData("Restaurants rate", $scope.chartSubDataRestaurant));
        $scope.currentRestourant = String($scope.chartDataRestaurant[0].label);
        $scope.GetCurrentRestaurant(String($scope.chartDataRestaurant[0].label));
        });
    //Get restaurant menu items by name(from <select>)
    $scope.GetCurrentRestaurant = function (name) {
        //Full dishes list
        $scope.chartDataMenu = [];
        //Dishes list with short names
        $scope.chartSubDataMenu = [];
        //Dishes list included in the top
        $scope.chartDataMenuTop = [];
        //Dishes list that are not included in the top
        $scope.chartDataMenuOther = [];
        $scope.count1 = 0;
        $http.post("/Manager/StatisticsAjax", { name: name })
            .success(function(data) {
            $scope.ordersItemTopList = data;
            for (var i = 0; i < $scope.ordersItemTopList.length; i++) {
                color = String(randomHexColor());
                $scope.chartDataMenu.push({
                    "label": String($scope.ordersItemTopList[i].Name),
                    "value": $scope.ordersItemTopList[i].Count,
                    "color": color
                });
            };
            //Sort dishes(descending)
            $scope.chartDataMenu.sort(function (obj1, obj2) {
                return obj2.value - obj1.value;
            });
            //Get Top dishes
            $scope.chartSubDataMenu = $scope.GetChartDataWithSubTitle($scope.chartDataMenu, chartTitleLength, chartMaxTitleLength);
            for (var k = 0; k < Math.min($scope.topCountMenu, $scope.chartDataMenu.length) ; k++) {
                $scope.chartDataMenuTop[k] = $scope.chartDataMenu[k];
            };
            //Get dishes that are not included in the top
            for (var l = $scope.topCountMenu; l < $scope.chartDataMenu.length; l++) {
                $scope.chartDataMenuOther.push($scope.chartDataMenu[l]);
            };
            $scope.othersLengthMenu = $scope.chartDataMenuOther.length;
            var attr = $('#pieChart2 svg');

            if (attr.length > 0)
            {
                attr.remove();
            }
            //Draw dishes chart
            var pie = new d3pie("pieChart2", SetChartData("", $scope.chartSubDataMenu));
            //Show/Hide link to modal window
            if ($scope.chartDataMenu.length <= $scope.topCountMenu) {
                $('#othersTitle1').css("visibility", "hidden");
            } else {
                $('#othersTitle1').css("visibility", "visible");
            }
        });
    };
    //Get restaurant menu items by click on the name of the restaurant
    $scope.GetRestaurantNameForDishesRate = function(name) {
        $scope.currentRestourant = name;
        $scope.GetCurrentRestaurant(name);
    };
    $scope.Colored = function (name1, name2, mass, count) {
        for (var k = 0; k < Math.min(count, mass.length); k++) {
            $("#" + name1 + k).css("background", mass[k].color);
        }
        var b = 0;
        for (var z = count; z < mass.length; z++) {
            $("#" + name2 + b).css("background", mass[z].color);
            b++;
        }
    };
    $scope.GetChartDataWithSubTitle = function (mass, titleLength, maxTitleLength) {
        var newMass = [];
        for (var j = 0; j < mass.length; j++) {
            if (mass[j].label.length > maxTitleLength) {
                var label = mass[j].label.substring(0, titleLength) + "..";
            } else {
                label = mass[j].label;
            }
            newMass.push({
                "label": label,
                "value": mass[j].value,
                "color": mass[j].color
            });
        }
        return newMass;
    };

    $scope.ShowModalRestaurant = function() {
        $('#myModal').foundation('reveal', 'open');
    };

    $scope.ShowModalMenu = function () {
        $('#myModal1').foundation('reveal', 'open');
    };

    $scope.count = 0;
    $scope.afterLoadRestaurant = function () {
        if ($scope.chartDataRestaurant !== undefined) {
            $scope.count++;
        }
        
        if ($scope.count == $scope.chartDataRestaurant.length) {
            $timeout(function () {
                $scope.Colored("rect", "rect1", $scope.chartDataRestaurant, $scope.topCountRestaurant);
            }, 0);
            if ($scope.chartDataRestaurant.length <= $scope.topCountRestaurant) {
                $('#othersTitle').css("visibility", "hidden");
            }
        }
    };
    $scope.count1 = 0;
    $scope.afterLoadMenu = function () {
        if ($scope.chartDataMenu !== undefined) {
            $scope.count1++;
        }
        if ($scope.count1 === $scope.chartDataMenu.length) {
            $timeout(function () {
                $scope.Colored("rectMenu", "rectMenu1", $scope.chartDataMenu, $scope.topCountMenu);
            }, 0);
            
        }
    };
});

function randomHexColor() {
    var hexColor = []; 
    hexColor[0] = "#"; //first value of array needs to be hash tag for hex color val, could also prepend this later

    for (i = 1; i < 7; i++) {
        var x = Math.floor((Math.random() * 16)); //Tricky: Hex has 16 numbers, but 0 is one of them

        if (x >= 10 && x <= 15) //hex:0123456789ABCDEF, this takes care of last 6 
        {
            switch (x) {
            case 10:
                x = "a"
                break;
            case 11:
                x = "b"
                break;
            case 12:
                x = "c"
                break;
            case 13:
                x = "d"
                break;
            case 14:
                x = "e"
                break;
            case 15:
                x = "f"
                break;
            }
        }
        hexColor[i] = x;
    }
    var cString = hexColor.join(""); //this argument for join method ensures there will be no separation with a comma
    return cString;
};

function SetChartData(title, somedata) {
    var pieData = {
        "header": {
            "title": {
                "text": title,
                "fontSize": 24,
                "font": "open sans"
            },
            "subtitle": {
                "color": "#999999",
                "fontSize": 12,
                "font": "open sans"
            },
            "titleSubtitlePadding": 9
        },
        "footer": {
            "color": "#999999",
            "fontSize": 10,
            "font": "open sans",
            "location": "bottom-left"
        },
        "size": {
            "canvasWidth": 530,
            "canvasHeight": 370,
            "pieInnerRadius": "9%",
            "pieOuterRadius": "90%"
        },
        "data": {
            "sortOrder": "value-desc",
            "content": somedata
        },
        "labels": {
            "outer": {
                "pieDistance": 20
            },
            "inner": {
                "hideWhenLessThanPercentage": 3
            },
            "mainLabel": {
                "fontSize": 11
            },
            "percentage": {
                "color": "#ffffff",
                "decimalPlaces": 0
            },
            "value": {
                "color": "#adadad",
                "fontSize": 11
            },
            "lines": {
                "enabled": true
            }
        },
        "tooltips": {
            "enabled": true,
            "type": "placeholder",
            "string": "{label}: {value}, {percentage}%"
        },
        "effects": {
            "pullOutSegmentOnClick": {
                "effect": "linear",
                "speed": 400,
                "size": 8
            }
        },
        "misc": {
            "gradient": {
                "enabled": true,
                "percentage": 100
            }
        }
    };
    return pieData;
};