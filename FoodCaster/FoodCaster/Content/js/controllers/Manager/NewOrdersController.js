﻿ManagerModule.controller('NewOrdersController', function($scope, $http, $location) {
    document.title = "New orders";
    var result = $http.get("/Manager/NewOrdersAjax");
    result.success(function(data) {
        $scope.orders = data.reverse();
    });

    //Show/Hide and load MenuItems to the session storage after click on current order item
    $scope.toggleDetail = function($index, ordrId) {
        for (var i = 0; i < $scope.orders.length; i++) {
            if ($scope.orders[i].Id == ordrId) {
                $scope.ord_id = ordrId;
            }
        }
        for (var j = 0; j < sessionStorage.length; j++) {
            if (sessionStorage.key(j) == 'key' + [$scope.ord_id]) {
                $scope.key = sessionStorage.key(j);
            }
        }
        if ($scope.activePosition != $index) {
            if ($scope.key == 'key' + [$scope.ord_id]) {
                $scope.itemList = JSON.parse(sessionStorage.getItem('key' + [$scope.ord_id]));
            } else {
                var res = $http.post("/Manager/OrderItems", { orderId: $scope.ord_id });
                res.success(function(data) {
                    $scope.itemList = data.itemsList;
                    sessionStorage.setItem('key' + [$scope.ord_id], JSON.stringify(data.itemsList));
                });
            }
        }
        $scope.activePosition = $scope.activePosition == $index ? -1 : $index;
    };

    $scope.ApplyNewOrder = function(index) {
        $http.post("/Manager/ApplyNewOrder", { OrderId: $scope.orders[index].Id })
            .success(function(data) {
                var messageText = "";
                if (Boolean(data.success)) {
                    messageText = '<p><h4 class="notificationSuccessTitle">Applied</h4></p><p>' + data.message + '</p>';
                    $scope.orders.splice(index, 1);
                } else {
                    messageText = '<p><h4 class="notificationErrorTitle">Error</h4></p><p>' + data.message + '</p>';
                }
                showNotification(messageText);
                if (!$scope.$$phase) {
                    scope.$apply();
                }
            });
    };

    $scope.AbortedNewOrder = function(index) {
        $http.post("/Manager/AbortedNewOrder", { OrderId: $scope.orders[index].Id })
            .success(function(data) {
                var messageText = "";
                if (Boolean(data.success)) {
                    messageText = '<p><h4 class="notificationSuccessTitle">Aborted</h4></p><p>' + data.message + '</p>';
                    $scope.orders.splice(index, 1);
                } else {
                    messageText = '<p><h4 class="notificationErrorTitle">Error</h4></p><p>' + data.message + '</p>';
                }
                showNotification(messageText);
                if (!$scope.$$phase) {
                    scope.$apply();
                }
            });
    };
});