﻿ManagerModule.controller('RestaurantsController', ['$scope', '$upload', '$http', function ($scope, $upload, $http) {
    document.title = "Restaurants";
    //true if click ok in modal window
    var ismodalok = false;
    //Object for saving data from the view
    $scope.temp = {};
    //Array for saving objects in one page
    $scope.items = {};
    //Array for saving all objects
    $scope.restaurants = {};
    //Data for search list of restaurants
    $scope.searchData = {};
    //Options for set items count in one page
    $scope.countVariants = [10, 20, 30, 50];
    //Set count of items in one page
    $scope.countItems = 10;
    //Set default current page
    $scope.currentPage = 0;
    //Set default (how many pages)
    $scope.pageCount = 5;
    //Set max count of pagination links list
    $scope.viewLinks = 5;
    //Last page number for button "Last>>"
    $scope.lastPage = 10;
    $scope.Oldlogo = "/Content/img/noimage.jpg";
    //upload
    $scope.upload = [];
    $scope.onFileSelect = function($files) {
        //$files: an array of files selected, each file has name, size, and type.
        for (var i = 0; i < $files.length; i++) {
            var $file = $files[i];
            (function(index) {
                $scope.upload[index] = $upload.upload({
                    url: "/Manager/AddRestaurantLogo", // webapi url
                    method: "POST",
                    data: { uploadedFile: $file },
                    file: $file
                }).success(function(data, status, headers, config) {
                    if (data.success) {
                        $scope.OldLogo = $scope.currentRestaurant.Logo;
                        $scope.currentRestaurant.Logo = data.Logourl;
                    }
                }).error(function(data, status, headers, config) {
                    // file failed to upload
                    showNotification('Error occured during upload');
                });
            })(i);
        }
    };
    // Getting Restaurant by Id
    $scope.getRestaurantById = function (id) {
        for (i = 0; i < $scope.restaurants.length; i++) {
            if ($scope.restaurants[i].Id == id) {
                var r = $scope.restaurants[i];
                return r;
            }
        }
        return null;
    };
    //Edit user Modal init
    $scope.ShowEditRestaurant = function (id) {

        var temp = $scope.getRestaurantById(id);
        $scope.currentRestaurant = {};
        $scope.currentRestaurant.Id = temp.Id;
        $scope.currentRestaurant.Name = temp.Name;
        $scope.currentRestaurant.Location = temp.Location;
        $scope.currentRestaurant.Category = temp.Category;
        $scope.currentRestaurant.Logo = temp.Logo;
        $scope.currentRestaurant.Status = temp.Status;
        $scope.OldPhoto = $scope.currentRestaurant.Logo;
        $('#ModalTitle').html("Edit restaurant data (id = " + id + ")");
        $('#Action').html("Change restaurant data");
        $("div.error").attr("class", "small-5 small-pull-3 columns");
        $("label.error").attr("class", "right inline");
        $('#modalRestaurant').foundation('reveal', 'open');
    };
    //Remove dinningLogo from cancelled modal window 
    $('#modalRestaurant').on('closed', function () {
        if ($scope.currentRestaurant.Logo != "/Content/img/noimage.jpg") {           
            if ($scope.currentRestaurant.Logo != $scope.OldPhoto) {              
                if (ismodalok == false) {
                    $.post("/Manager/DeleteDinningLogo", {
                        FileName: $scope.currentRestaurant.Logo
                    }).done(             
                $scope.currentRestaurant.Logo = $scope.OldPhoto);                 
                    //$scope.$apply();
                }
            }
        }
        ismodalok = false;
    });
    $scope.ShowAddRestaurant = function () {

        $scope.currentRestaurant = {};
        $scope.currentRestaurant.Logo = "/Content/img/noimage.jpg";
        $('#ModalTitle').html("Add new restaurant");
        $('#Action').html("Add restaurant");
        $("div.error").attr("class", "small-5 small-pull-3 columns");
        $("label.error").attr("class", "right inline");
        document.getElementById("uploadingLogo").value = "";
        $('#modalRestaurant').foundation('reveal', 'open');

    };
    $scope.ModalOk = function () {
        if ($("div.error").length == 0) {
            if ($scope.currentRestaurant.Id == null) {
                $.post("../Manager/AddNewRestaurant",
           {
               "Name": $scope.currentRestaurant.Name,
               "Location": $scope.currentRestaurant.Location,
               "Category": $scope.currentRestaurant.Category,
               "Status": $scope.currentRestaurant.Status,
               "Logo": $scope.currentRestaurant.Logo,
           })

       .done(function (data) {
           var messageText = "";
           if (Boolean(data.success)) {
               ismodalok = true;
               $('#modalRestaurant').foundation('reveal', 'close');
               messageText = '<p><h4 class="notificationSuccessTitle">Added</h4></p><p>' + data.message + '</p>';
               $scope.restaurants.push({
                   Id: data.id,
                   Name: $scope.currentRestaurant.Name,
                   Location: $scope.currentRestaurant.Location,
                   Category: $scope.currentRestaurant.Category,
                   Status: $scope.currentRestaurant.Status,
                   Logo: "/Content/img/noimage.jpg",
               });
               document.getElementById("uploadingLogo").value = "";
               $scope.RebuildPages();
               $scope.currentRestaurant = {};
           } else {
               messageText = '<p><h4 class="notificationErrorTitle">Error</h4></p><p>' + data.message + '</p>';
           };
           showNotification(messageText);
           $scope.$apply();
       });

            } else {

                $.post("../Manager/ChangeRestaurantData",
                                {
                                    "Id": $scope.currentRestaurant.Id,
                                    "Name": $scope.currentRestaurant.Name,
                                    "Location": $scope.currentRestaurant.Location,
                                    "Status": $scope.currentRestaurant.Status,
                                    "Category": $scope.currentRestaurant.Category,
                                    "Logo": $scope.currentRestaurant.Logo,
                                })
                                .done(function (data) {
                                    ismodalok = true;
                                    var messageText = "";
                                    if (Boolean(data.success)) {
                                        var temp = $scope.getRestaurantById($scope.currentRestaurant.Id);
                                        temp.Name = $scope.currentRestaurant.Name;
                                        temp.Location = $scope.currentRestaurant.Location;
                                        temp.Category = $scope.currentRestaurant.Category;
                                        temp.Status = $scope.currentRestaurant.Status;
                                        temp.Logo = $scope.currentRestaurant.Logo;
                                        $('#modalRestaurant').foundation('reveal', 'close');
                                        document.getElementById("uploadingLogo").value = "";
                                        $scope.RebuildPages();
                                        messageText = '<p><h4 class="notificationSuccessTitle">Changes successfully saved</h4></p><p>' + data.message + '</p>';
                                    } else {
                                        messageText = '<p><h4 class="notificationErrorTitle">Error</h4></p><p>' + data.message + '</p>';
                                    };
                                    showNotification(messageText);
                                    $scope.$apply();
                                });
            };
        } else {
            messageText = '<p><h4 class="notificationErrorTitle">Error</h4></p><p>Data in form is not valid</p>';
            showNotification(messageText);
        };
    };
    //Delete current item from server and angular model
    $scope.Delete = function (index) {
        var pageIndex = index + $scope.currentPage * $scope.countItems;
        $.post("/Manager/DeleteRestaurant", { id: $scope.restaurants[pageIndex].Id })
            .done(function (data) {
                var messageText = "";
                if (Boolean(data.success)) {
                    messageText = '<p><h4 class="notificationSuccessTitle">Deleted</h4></p><p>' + data.message + '</p>';
                    $scope.restaurants.splice(pageIndex, 1);
                    $scope.setPage($scope.currentPage);
                } else {
                    messageText = '<p><h4 class="notificationErrorTitle">Error</h4></p><p>' + data.message + '</p>';
                }
                showNotification(messageText);
                $scope.$apply();
            });
    };
    //Redirect to show and edit view menu of current restaurant by index of item in current page
    $scope.Menu = function (index) {
        ManagerModule.menuItemId = index;
        window.open("#/MenuItems", "_self");
    };
    //Get pagination links int array (1,2,3..)
    $scope.range = function () {
        var rangeSize = $scope.viewLinks;
        var ret = [];
        var start = 0;
        if ($scope.currentPage > $scope.viewLinks - 2) {
            start = $scope.currentPage;
        }
        for (var i = start; i < start + rangeSize; i++) {
            if (i > ($scope.restaurants.length - 1) / $scope.countItems) continue;
            ret.push(i);
        }
        $scope.pageCount = ret.length - 1;
        return ret;
    };
    //Show current page + next page and prev page
    $scope.setPage = function (n) {
        $scope.lastPage = Math.ceil(($scope.restaurants.length) / $scope.countItems) - 1;
        if (n >= 0) {
            if (n > $scope.lastPage) { $scope.currentPage = $scope.lastPage; }
            else { $scope.currentPage = n; }
            $scope.items = {};
            for (i = 0; i < $scope.countItems; i++) {
                if ($scope.restaurants[i + $scope.currentPage * $scope.countItems] == null) continue;
                $scope.items[i] = $scope.restaurants[i + $scope.currentPage * $scope.countItems];
            }
        };
    };
    $scope.prevPage = function () {
        if ($scope.currentPage > 0) {
            $scope.currentPage--;
            $scope.setPage($scope.currentPage);
        }
    };
    $scope.nextPage = function () {
        if ($scope.currentPage < $scope.pageCount) {
            $scope.currentPage++;
            $scope.setPage($scope.currentPage);
        }
    };
    $scope.RebuildPages = function () {
        $scope.countItems = 10;
        if ($scope.countItems > $scope.restaurants.length) {
            $scope.countItems = $scope.restaurants.length;
        }
        for (i = 0; i < $scope.countItems; i++) {
            $scope.items[i] = $scope.restaurants[i];
        }
        $scope.lastPage = Math.ceil(($scope.restaurants.length) / $scope.countItems) - 1;
        $scope.setPage($scope.currentPage);
        $scope.searchData = {};
    };
    //Get data from server after load all html page and update angular model
    $http.post("/Manager/RestaurantsAjax",{model:null})
    .success(function (data) {
        $scope.restaurants = data.DinningList;
        $scope.Category = data.CategoryList;
        $scope.dinningStatuses = data.StatusList;
        $scope.RebuildPages();
    });
    //Method for searc restaurants in server by searchData
    $scope.SearchRest = function () {
        $http.post("/Manager/RestaurantsAjax", {
                    Name: $scope.searchData.Name,
                    Location: $scope.searchData.Location,
                    Category: $scope.searchData.Categories,
                    Status: $scope.searchData.Status
            })
        .success(function (data) {
            $scope.restaurants = data.DinningList;
            $scope.RebuildPages();
        });
    }
}]);
