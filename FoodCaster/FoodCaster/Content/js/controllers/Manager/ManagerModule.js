﻿var ManagerModule = angular.module('ManagerModule', ['angularFileUpload']);

var menuItemId;

ManagerModule.config(['$routeProvider','$locationProvider',
  function ($routeProvider, $locationProvider) {
      $routeProvider
          .when('/', {
              templateUrl: 'NewOrders',
              controller: 'NewOrdersController'
          })
          .when('/NewOrders', {
              templateUrl: 'NewOrders',
              controller: 'NewOrdersController'
          })
          .when('/ManageOrders', {
              templateUrl: 'ManageOrders',
              controller: 'ManageOrdersController'
          })
          .when('/Restaurants', {
              templateUrl: 'Restaurants',
              controller: 'RestaurantsController'
          })
          .when('/MenuItems', {
              templateUrl: 'MenuItems',
              controller: 'MenuItemsController'
          })
          .when('/Statistics', {
              templateUrl: 'Statistics',
              controller: 'StatisticsController'
          })
      .otherwise({
          redirectTo: '/'
      });
  }]);