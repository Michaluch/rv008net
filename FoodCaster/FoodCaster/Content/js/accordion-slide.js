﻿$(".accordion").on("click", 'dd a', function () {
    if ($(this).parent().hasClass('active') && $(this).parent().find(".content").css("display") == "none") {
        $(this).parent().find(".content").slideToggle("slow");
    } else {
        if ($(this).parent().hasClass('active') && $(this).parent().find(".content").css("display") == "block") {
            $(this).parent().removeClass('active').find(".content").slideUp("slow");
        } else {
            $(this).parent().addClass('active').find(".content").slideToggle("slow");
        }
    }
});