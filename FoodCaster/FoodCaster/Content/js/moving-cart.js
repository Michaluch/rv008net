﻿function drag_start(event) {
    var style = window.getComputedStyle(event.target, null);
    event.dataTransfer.setData("text/plain",
    (parseInt(style.getPropertyValue("left"), 10) - event.clientX) + ',' + (parseInt(style.getPropertyValue("bottom"), 10) + event.clientY));
}
function drag_over(event) {
    event.preventDefault();
    return false;
}
function drop(event) {
    var offset = event.dataTransfer.getData("text/plain").split(',');
    var bd = document.body;
    var maxBodyHeight = Math.max(bd.scrollHeight, bd.clientHeight);
    var heightwithScroll = bd.clientHeight - maxBodyHeight;
    var height = bd.clientHeight - el.clientHeight;
    var bottom = (parseInt(offset[1], 10) - event.clientY);

    var left = (event.clientX + parseInt(offset[0], 10)); 
    var width = window.innerWidth - el.clientWidth - (window.innerWidth/75);
    
    if (left < 10) {
        el.style.left = 1 + '%';
    } else {
        if (left >= width) {
            el.style.left = width + 'px';
        } else {
            el.style.left = left + 'px';
        }
    }
    if ((bottom < heightwithScroll) && (bd.clientHeight == maxBodyHeight)) {
        el.style.bottom = 1 + '%';
    } else {
        if ((bottom < heightwithScroll) && (bd.clientHeight != maxBodyHeight)) {
            el.style.bottom = heightwithScroll + 10 + 'px';
        } else {
            if (bottom >= height) {
                el.style.bottom = height + 'px';
            } else {
                el.style.bottom = bottom + 'px';
            }
        }
    }
    event.preventDefault();
    return false;
}
var el = document.getElementById('moving');
el.addEventListener('dragstart', drag_start, false);
document.body.addEventListener('dragover', drag_over, false);
document.body.addEventListener('drop', drop, false);

window.onresize = function() {
    el.style.bottom = 1 + '%';
    el.style.left = 1 + '%';
}