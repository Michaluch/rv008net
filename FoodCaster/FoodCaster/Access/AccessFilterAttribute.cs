﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using FoodCaster.Bussiness.Common.Enums;
using FoodCaster.Bussiness.Common.StaticStrings;

namespace FoodCaster.Access
{
    /// <summary>
    /// Attribute that used for redirect to the login page 
    /// if the role of user does not allow to make a request
    /// </summary>
    public class AccessFilterAttribute : AuthorizeAttribute
    {
        public RoleType Role;

        public AccessFilterAttribute(RoleType Role)
        {
            this.Role = Role;
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            //return true;// just for testing
            // disables validation
            try
            {
                string role = httpContext.Session[SessionKeys.Role].ToString();
                return string.Equals(Role.ToString(), role);
            } catch(Exception)
            {
                return false;
            }
        }
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            // Returns HTTP 401 - see comment in HttpUnauthorizedResult.cs.
            filterContext.Result = new RedirectToRouteResult(
                                       new RouteValueDictionary 
                                   {
                                       { "action", AuthorizationView.LogIn},
                                       { "controller", ControllersEnum.Authorization }
                                   });
        }
    }
}