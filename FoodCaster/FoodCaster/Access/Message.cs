﻿namespace FoodCaster.Access
{
    /// <summary>
    /// Class used to transfer message to an error page 
    /// </summary>
    public static class Message
    {
        public static bool Success {get; set;}
        public static string Text {get; set;}

        public static void Set(bool success, string text)
        {
            Success = success;
            Text = text;
        }
    }
}