﻿using System.Web.Mvc;
using System.Web.Routing;

namespace FoodCaster
{
    /// <summary>
    /// Сlass represents the route configuration 
    /// </summary>
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Authorization", action = "Registration", id = UrlParameter.Optional }
            );
        }
    }
}
