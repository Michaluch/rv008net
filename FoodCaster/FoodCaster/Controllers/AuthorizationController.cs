﻿using FoodCaster.Bussiness.Common.Enums;
using FoodCaster.Bussiness.Common.StaticStrings;
using System.Web.Mvc;
using FoodCaster.Bussiness.Models;
using FoodCaster.Bussiness.Services;
using Message = FoodCaster.Access.Message;

namespace FoodCaster.Controllers
{
    /// <summary>
    /// Class (controller) used by all users for registration and login
    /// </summary>
    public class AuthorizationController : Controller
    {
        //Services initialization 
        private readonly LoginService _service = new LoginService();
        public ActionResult Index()
        {
            return RedirectToAction(AuthorizationView.Registration, ControllersEnum.Authorization);
        }

        public ActionResult LogIn()
        {
            Session.Clear();
            return View(AuthorizationView.LogIn);
        }

        [HttpPost]
        public ActionResult LogIn(LoginModel jsonModel)
        {
            var user = _service.UserAuthorization(jsonModel);
            if (user != null)
            {
                Session.Add(SessionKeys.Name, user.UserName);
                Session.Add(SessionKeys.Role, user.Role.Name);
                Session.Add(SessionKeys.Login, user.Email);
                Session.Add(SessionKeys.Id, user.Id);
                return SessionRedirect();
            }
            Message.Set(_service.Success, _service.Message);
            return View(SharedView.MessagePage);
        }

        public ActionResult Registration()
        {
            if (Session.Count > 0)
            {
                return SessionRedirect();
            }
            return View();
        }

        [HttpPost]
        public ActionResult RegisterNewUser(LoginModel jsonModel)
        {
            _service.SaveChanges(jsonModel, OperationType.Add);
            Message.Set(_service.Success, _service.Message);
            return View(SharedView.MessagePage);
        }

        private ActionResult SessionRedirect()
        {
            if (Session[SessionKeys.Role].ToString() == RoleType.Admin.ToString())
                return RedirectToAction(AdminView.Index, ControllersEnum.Admin);
            if (Session[SessionKeys.Role].ToString() == RoleType.User.ToString())
                return RedirectToAction(UserView.Index, ControllersEnum.User);
            if (Session[SessionKeys.Role].ToString() == RoleType.Manager.ToString())
                return RedirectToAction(ManagerView.Index, ControllersEnum.Manager);
            return Content("Data base have no this role");
        }
    }
}