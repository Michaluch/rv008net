﻿using System.Web.Mvc;
using FoodCaster.Access;
using FoodCaster.Bussiness.Common.Enums;
using FoodCaster.Bussiness.Models;
using FoodCaster.Bussiness.Services;


namespace FoodCaster.Controllers
{
    /// <summary>
    /// Class (controller) used by admins to work with users
    /// Has the ability to add, delete, update and get users data
    /// </summary>
    [AccessFilter(RoleType.Admin)]
    public class AdminController:Controller
    {
        //Services initialization 
        private readonly UserService _service = new UserService();

        //return home page for admin
        public ActionResult Index()
        {
            return View();
        }

        #region Users
        //return users view for admin
        public ActionResult Users()
        {
            return View();
        }

        //Add new user by user model
        [HttpPost]
        public ActionResult AddNewUser(UserModel jsonUser)
        {
            _service.SaveChanges(jsonUser, OperationType.Add);
            return RetunActionJson(jsonUser);        
        }

        //Change user data user by user model
        [HttpPost]
        public ActionResult ChangeUserData(UserModel jsonUser)
        {
            _service.SaveChanges(jsonUser, OperationType.Update);
            return RetunActionJson(jsonUser);
        }

        //Delete user data user by user model
        [HttpPost]
        public ActionResult UserDelete(UserModel jsonUser)
        {
            _service.SaveChanges(jsonUser, OperationType.Delete);
            return RetunActionJson(jsonUser);
        }

        //Search users and return list of users (ajax)
        [HttpPost]
        public ActionResult UsersAjax(UserModel model = null)
        {
            return Json(new { UsersList = _service.GetItems(model), RolesList = _service.GetRoles() }, JsonRequestBehavior.AllowGet);
        }

        //Method for make json object for return after service operation
        private ActionResult RetunActionJson(UserModel jsonUser)
        {
            return _service.Success ?
                Json(new { addedUser = jsonUser, success = _service.Success, message = _service.Message }) :
                Json(new { success = _service.Success, message = _service.Message });
        }
        #endregion
    }
}