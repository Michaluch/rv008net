﻿using FoodCaster.Access;
using FoodCaster.Bussiness.Common.Enums;
using FoodCaster.Bussiness.Models;
using FoodCaster.Bussiness.Services;
using System;
using System.Web;
using System.Web.Mvc;
using FoodCaster.Bussiness.Validation;


namespace FoodCaster.Controllers
{
    /// <summary>
    /// Class (controller) used by managers to work with users
    /// Has the ability to add, delete, update and get dinning and dinning menu data,
    /// used for manage orders by manager and show statistic
    /// </summary>
    [AccessFilter(RoleType.Manager)]
    public class ManagerController : Controller
    {
        //Services initialization 
        private readonly MenuItemService _menuitemService = new MenuItemService();
        private readonly DinningService _dinningService = new DinningService();
        private readonly ManagerStatisticService _managerStatisticService = new ManagerStatisticService();
        private readonly ManagerOrderService _orderService = new ManagerOrderService();
        public ActionResult Index()
        {
            Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            Response.Cache.SetNoStore();
            return View();
        }
        public ActionResult ManageMenu()
        {
            return View();
        }

        #region Orders
        
        [HttpGet]
        public ActionResult NewOrders()
        {  
            return View();
        }

        [HttpGet]
        public ActionResult NewOrdersAjax()
        {
            return Json(_orderService.GetOrder(OrderStatus.Pending.ToString()), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult OrderItems(int orderId)
        {
            return Json(new { itemsList = _orderService.GetItemList(orderId) });
        }

        public ActionResult ManageOrders()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ManageOrdersAjax(string status)
        {
            return Json(new { orderList = _orderService.GetOrder(status) });
        }

        [HttpPost]
        public ActionResult ApplyNewOrder(int orderId)
        {
            _orderService.AddOrderTrackInfo(orderId, OrderStatus.Confirmed);
            return Json(new { success = _orderService.Success, message = _orderService.Message});
        }

        [HttpPost]
        public ActionResult AbortedNewOrder(int orderId)
        {
            _orderService.AddOrderTrackInfo(orderId, OrderStatus.Aborted);
            return Json(new { success = _orderService.Success, message = _orderService.Message });
        }

        [HttpPost]
        public ActionResult CompleteOrder(int orderId)
        {
            _orderService.AddOrderTrackInfo(orderId, OrderStatus.Completed);
            return Json(new { success = _orderService.Success, message = _orderService.Message });
        }
        #endregion
        
        #region MenuItems
        
        public ActionResult MenuItems()
        {
            return View();
        }

        public ActionResult MenuItemsAjax(int diningId)
        {
            return Json(_menuitemService.GetItems(diningId,true), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult MenuItemDelete(MenuItemModel model)
        {
            _menuitemService.SaveChanges(model, OperationType.Delete);
            return Json(new { success = _menuitemService.Success, message = _menuitemService.Message });
        }

        [HttpPost]
        public ActionResult MenuItemAdd(MenuItemModel model)
        {
            _menuitemService.SaveChanges(model, OperationType.Add);
            return _menuitemService.Success ?
                Json(new { id = model.Id, success = _menuitemService.Success, message = _menuitemService.Message }) :
                Json(new { success = _menuitemService.Success, message = _menuitemService.Message });
        }

        [HttpPost]
        public ActionResult MenuItemEdit(MenuItemModel model)
        {
            _menuitemService.SaveChanges(model, OperationType.Update);
            return _menuitemService.Success ?
                Json(new { id = model.Id, success = _menuitemService.Success, message = _menuitemService.Message }) :
                Json(new { success = _menuitemService.Success, message = _menuitemService.Message });
        }
        #endregion   

        #region Restaurants
        
        public ActionResult Restaurants()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ChangeRestaurantData(DinningModel model)
        {
            _dinningService.SaveChanges(model, OperationType.Update);
            return _dinningService.Success ?
            Json(new { id = model.Id, success = _dinningService.Success, message = _dinningService.Message }) :
            Json(new { success = _dinningService.Success, message = _dinningService.Message });
        }    

        [HttpPost]
        public ActionResult RestaurantsAjax(DinningModel model=null)
        {
             return Json(new
             {
                 DinningList=_dinningService.GetItems(model,true),
                 CategoryList=_dinningService.GetCategoryList(true),
                 StatusList = _dinningService.GetStatusList()
             });
        }

        public ActionResult AddNewRestaurant(DinningModel model)
        {
            _dinningService.SaveChanges(model, OperationType.Add);
             return _dinningService.Success ?
             Json(new { id = model.Id, success = _dinningService.Success, message = _dinningService.Message }) :
             Json(new { success = _dinningService.Success, message = _dinningService.Message });
        }

        public ActionResult AddRestaurantLogo(HttpPostedFileBase uploadedFile)
        {
            try
            {
                string logo = CheckImage.CheckImgFile(Server.MapPath("~/Content/img/"), "restaurantsLogo/", uploadedFile);
                return Json(new { success = true, message = "success", Logourl = logo });
            }
            catch (DataValidationException ex)
            {
                return Json(new { success = false, message = ex.Message });
            }
            catch (Exception)
            {
                return Json(new { success = false, message = "fail" });
            }
        }
        [HttpPost]
        public void DeleteDinningLogo(string FileName)
        {
            System.IO.File.Delete(Server.MapPath(("~") + FileName));
        }

        public ActionResult DeleteRestaurant(DinningModel model)
        {
            _dinningService.SaveChanges(model, OperationType.Delete);
            return Json(new { success = _dinningService.Success, message = _dinningService.Message });
        }
        #endregion Restaurants

        #region Statistics      
        public ActionResult Statistics()
        {
            return View();
        }

        [HttpGet]
        public ActionResult StatisticsAjax()
        {
            return Json(_managerStatisticService.GetDiningsRate(), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult StatisticsAjax(string name)
        {
            return Json(_managerStatisticService.GetDishesRate(name));
        }
        #endregion
    }
}