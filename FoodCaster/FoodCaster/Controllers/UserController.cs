﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using FoodCaster.Access;
using FoodCaster.Bussiness.Common.Enums;
using FoodCaster.Bussiness.Common.StaticStrings;
using FoodCaster.Bussiness.Models;
using FoodCaster.Bussiness.Services;
using FoodCaster.Bussiness.Validation;

namespace FoodCaster.Controllers
{
    /// <summary>
    /// Class (controller) used by user to work with profile, addresses and orders
    /// Gives the opportunity to make, abort, and reopen orders 
    /// </summary>
    [AccessFilter(RoleType.User)]
    public class UserController : Controller
    {
        //Services initialization 
        private readonly UserOrderService _orderService = new UserOrderService();
        private readonly UserService _userService = new UserService();
        private readonly DinningService _dinningService = new DinningService();
        private readonly MenuItemService _menuitemService = new MenuItemService();
        private readonly AddressService _addressService = new AddressService();

        public ActionResult Index()
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();
            return View();
        }
        #region User         
        public ActionResult ViewUser()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ViewUserAjax()
        {
            var user = new UserModel
            {
                Email = Session[SessionKeys.Login].ToString()
            };
            return Json(_userService.GetItem(user), JsonRequestBehavior.AllowGet);     
        }

        [HttpPost]
        public ActionResult EditUserData(UserModel model)
        {
            _userService.SaveChanges(model, OperationType.Update);
            return Json(new { success = _userService.Success, message = _userService.Message });         
        }

        public ActionResult AddPhotoLink(HttpPostedFileBase uploadedFile)
        {
            try
            {
                var photo = CheckImage.CheckImgFile(Server.MapPath("~/Content/img/"),"Photo/", uploadedFile);
                return Json(new { success = true, message = "success", Photourl = photo });
            }
            catch (Exception)
            {
                return Json(new { success = false, message = String.Format("fail") });
            }
        }

        [HttpPost]
        public void DeletePhotoLink(string FileName)
        {
            System.IO.File.Delete(Server.MapPath(("~") + FileName));
        }

        [HttpPost]
        public ActionResult LogOut()
        {
            return RedirectToAction(AuthorizationView.LogIn, ControllersEnum.Authorization);
        }

        #endregion

        #region Orders
        public ActionResult MyOrders()
        {
            return View(UserView.MyOrders);
        }

        public ActionResult MyOrdersAjax(int ordersLoaded)
        {
            var userId = (int)Session[SessionKeys.Id];
            return Json(new { OrdersList = _orderService.GetOrders(userId, false, ordersLoaded), AllOrdersLoaded = _orderService.AllOrdersLoaded }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CompletedOrders()
        { 
            return View(UserView.MyOrders);
        }

        public ActionResult CompletedOrdersAjax(int ordersLoaded)
        {
            var userId = (int)Session[SessionKeys.Id];
            return Json(new { OrdersList = _orderService.GetOrders(userId, true, ordersLoaded), AllOrdersLoaded = _orderService.AllOrdersLoaded }, JsonRequestBehavior.AllowGet); 
        }

        [HttpPost]
        public ActionResult AbortOrder(int orderId)
        {
            var managerOrderService = new ManagerOrderService();
            managerOrderService.AddOrderTrackInfo(orderId, OrderStatus.Aborted,"user");
            return Json(new { success = managerOrderService.Success, message = managerOrderService.Message });
        }

        [HttpPost]
        public ActionResult AddNewOrder(int addressId, bool returnNewOrderData, List<OrderItemModel> itemsIdList)
        {
            var userId = (int)Session[SessionKeys.Id];
            return Json(new 
            {
                newOrder = _orderService.AddNewOrder(userId, addressId, returnNewOrderData, itemsIdList),
                success = _orderService.Success,
                message = _orderService.Message
            });
        }

        [HttpPost]
        public ActionResult ReopenOrder(int orderId) 
        {
            return Json(new
            {
                orderItems = _orderService.ReopenOrder(orderId),
                success = _orderService.Success,
                message = _orderService.Message
            });
        }

        #endregion

        #region Addresses

        public ActionResult Addresses()
        {
            return View();
        }

        public ActionResult AddressesAjax()
        {
            var userId = (int)Session[SessionKeys.Id];
            return Json(new { addresses = _addressService.GetItems(userId), UserId=userId });
        }

        [HttpPost]
        public ActionResult AddAddress(AddressModel model)
        {
            _addressService.SaveChanges(model, OperationType.Add);
            return Json(new { success = _addressService.Success, message = _addressService.Message, newAddress=model });
        }

        [HttpPost]
        public ActionResult DeleteAddress(AddressModel model)
        {
            _addressService.SaveChanges(model,OperationType.Delete);
            return Json(new { success = _addressService.Success, message = _addressService.Message});
        }

        [HttpPost]
        public ActionResult EditAddress(AddressModel model)
        {
            _addressService.SaveChanges(model,OperationType.Update);
            return Json(new { success = _addressService.Success, message = _addressService.Message});
        }

        #endregion Addresses

        #region Dinings
        public ActionResult DiningMenuItems()
        {
            return View();
        }

        public ActionResult MyFoodCaster()
        { 
            return View();
        }

        public ActionResult MyFoodcasterAjax(DinningModel model=null)
        {
            return Json(new
            {
                DinningList = _dinningService.GetItems(model), 
                CategoryList = _dinningService.GetCategoryList()
            }, JsonRequestBehavior.AllowGet);  
        }
        
        [HttpGet]
        public ActionResult DiningMenuItemsAjax(int diningId)
        {
            return Json(_menuitemService.GetItems(diningId), JsonRequestBehavior.AllowGet);
        }     
        #endregion Dinings

    }
}
