﻿namespace FoodCaster.Bussiness.Common.Enums
{
    /// <summary>
    /// Enum to describe the roles using in application
    /// </summary>
    public enum RoleType
    {
        Admin,
        User,
        Manager
    }
}
