﻿namespace FoodCaster.Bussiness.Common.Enums
{
    /// <summary>
    /// Enum for order statuses
    /// Pending - newly created by user
    /// Confirmed - apply by manager (accepted for execution)
    /// Aborted - canceled by user or manager
    /// Completed - order was successful by manager
    /// </summary>
    public enum OrderStatus
    {
        Pending,
        Confirmed,
        Aborted,
        Completed
    }
}
