﻿namespace FoodCaster.Bussiness.Common.Enums
{
    /// <summary>
    /// Enums for check image formats
    /// </summary>
    public enum ImageFormats
    {
        jpeg,
        png,
        unknown
    }
}
