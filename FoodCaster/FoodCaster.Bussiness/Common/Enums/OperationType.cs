﻿namespace FoodCaster.Bussiness.Common.Enums
{
    /// <summary>
    /// Enums for check operation type. using in sevices
    /// for add, delete and update data
    /// </summary>
    public enum OperationType
    {
        Add,
        Update,
        Delete
    }
}
