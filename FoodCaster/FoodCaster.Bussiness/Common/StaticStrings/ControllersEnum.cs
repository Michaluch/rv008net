﻿namespace FoodCaster.Bussiness.Common.StaticStrings
{
    public static class ControllersEnum
    {
        public static string Admin = "Admin";
        public static string Authorization = "Authorization";
        public static string Manager = "Manager";
        public static string User = "User";
    }
}