﻿namespace FoodCaster.Bussiness.Common.StaticStrings
{
    public static class DinningStatuses
    {
        public static string Enabled = "Enabled";
        public static string Disabled = "Disabled";
    }
}
