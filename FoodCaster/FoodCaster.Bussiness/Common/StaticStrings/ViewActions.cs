﻿namespace FoodCaster.Bussiness.Common.StaticStrings
{
    public static class AuthorizationView
    {
        public static string LogIn = "LogIn";
        public static string Registration = "Registration";
    }
    public static class AdminView
    {
        public static string Index = "Index";
        public static string Users = "Users";
    }
    public static class UserView
    {
        public static string Index = "Index";
        public static string MyFoodCaster = "MyFoodCaster";
        public static string MyOrders = "MyOrders";
        public static string ViewUser = "ViewUser";
        public static string Addresses = "Addresses";
        public static string CompletedOrders = "CompletedOrders";
    }
    public static class ManagerView
    {
        public static string NewOrders = "NewOrders";
        public static string ManageOrders = "ManageOrders";
        public static string Restaurants = "Restaurants";
        public static string Index = "Index";
    }
    public static class SharedView
    {
        public static string MessagePage = "MessagePage";
    }
} 
