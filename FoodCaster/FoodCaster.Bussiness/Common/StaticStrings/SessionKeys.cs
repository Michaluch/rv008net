﻿namespace FoodCaster.Bussiness.Common.StaticStrings
{
    public static class SessionKeys
    {
        public const string Name = "Name";
        public const string Role = "Role";
        public const string Login = "Login";
        public const string Id = "Id";
    }
}