﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using FoodCaster.Bussiness.Common.Enums;
using FoodCaster.Bussiness.Models;

namespace FoodCaster.Bussiness.Validation
{
    /// <summary>
    /// Supports the method to validate BaseModel objects.
    /// </summary>
    /// <typeparam name="TModel">BaseModel type.</typeparam>
    public interface IValidator<in TModel> where TModel : BaseModel
    {
        /// <summary>
        /// Validates each property for BaseModel object.
        /// </summary>
        /// <param name="model">BaseModel to validate.</param>
        /// <returns>
        /// Collection ValidationResult objects with error and related property name or null if
        /// validation succeeded without any errors.
        /// </returns>
        IEnumerable<ValidationResult> DataValidate(TModel model);

        void OperationValidate(OperationType operation, TModel model);
    }
}