﻿using System;

namespace FoodCaster.Bussiness.Validation
{
    public class DataValidationException : Exception
    {
        public DataValidationException()
        { }

        public DataValidationException(string ex)
            : base(ex)
        { }

        public DataValidationException(string message, Exception innerException)
            : base(message, innerException)
        { }

        public DataValidationException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
            : base(info, context)
        { }
    }
}