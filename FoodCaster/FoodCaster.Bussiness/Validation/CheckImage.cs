﻿using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using FoodCaster.Bussiness.Common.Enums;

namespace FoodCaster.Bussiness.Validation
{
    public static class CheckImage
    {
        /// <summary>
        /// Method for check image by type and format 
        /// </summary>
        /// <param name="serverRootPath">Path to image file in server</param>
        /// <param name="imgPath">correct location</param>
        /// <param name="logo">image file</param>
        /// <returns>concatenation of path and img file string</returns>
        public static string CheckImgFile(string serverRootPath, string imgPath ,HttpPostedFileBase logo)
        {
            //Check image format by regexp
            if (!Regex.IsMatch(logo.ContentType, @"^image/gif|image/jpeg|image/png$"))
            {
                throw new DataValidationException("invalid logo type, use .gif .jpeg .png");
            }
            //Check image size
            if (logo.ContentLength > 1048576)
            {
                throw new DataValidationException("invalid logo, max size is 1 MB");
            }
            //Get name for image using data
            var fileName = DateTime.Now.ToString("MMyyyyhmmss-fff") + Path.GetExtension(logo.FileName);
            //Check image format using input stream of bytes
            using (var inputStream = logo.InputStream)
            {
                var memoryStream = inputStream as MemoryStream;
                if (memoryStream == null)
                {
                    memoryStream = new MemoryStream();
                    inputStream.CopyTo(memoryStream);
                }
                var data = memoryStream.ToArray();

                var result = GetImageFormat(data);
                if (result == ImageFormats.unknown)
                {
                    throw new DataValidationException("Wrong format or file is corrupted");
                }
                logo.SaveAs(Path.Combine(serverRootPath, imgPath, fileName));
            }
            return Path.Combine("/Content/img/" + imgPath, fileName);
        }
        /// <summary>
        /// Method for check image format 
        /// </summary>
        /// <param name="bytes">memory stream array of bytes</param>
        /// <returns>Image format as item of image formats enumeration</returns>
        public static ImageFormats GetImageFormat(byte[] bytes)
        { 
            var png = new byte[] { 137, 80, 78, 71 };    // PNG     
            var jpeg = new byte[] { 255, 216, 255, 224 }; // jpeg
            var jpeg2 = new byte[] { 255, 216, 255, 225 }; // jpeg canon

            if (png.SequenceEqual(bytes.Take(png.Length)))
                return ImageFormats.png;
            
            if (jpeg.SequenceEqual(bytes.Take(jpeg.Length)))
                return ImageFormats.jpeg;

            return jpeg2.SequenceEqual(bytes.Take(jpeg2.Length)) ? ImageFormats.jpeg : ImageFormats.unknown;
        }
    }
}