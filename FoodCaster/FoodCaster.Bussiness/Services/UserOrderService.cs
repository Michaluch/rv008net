﻿using System;
using System.Collections.Generic;
using System.Linq;
using FoodCaster.Bussiness.Common.Enums;
using FoodCaster.Bussiness.Common.StaticStrings;
using FoodCaster.Bussiness.Models;
using FoodCaster.Data.DataAccessLayer;
using FoodCaster.Data.Entities;

namespace FoodCaster.Bussiness.Services
{
    /// <summary>
    /// Service class, represents functionality to work with orders for users.
    /// </summary>
    public class UserOrderService : BaseService<OrderModel, Order, BaseVirtualDataAccessLayer<Order>>
    {
        public bool AllOrdersLoaded { get; set; }

        /// <summary>
        /// This method return list of orders for user
        /// </summary>
        /// <param name="userId">current user id</param>
        /// <param name="done">variable, which tell to show pending and confirmed orders (done = false), or completed and aborted orders (done = true) </param>
        /// <param name="ordersLoaded">count of loaded orders on orders page</param>
        /// <returns>list of orders</returns>
        public List<OrderModel> GetOrders(int userId, bool done, int ordersLoaded)
        {
            var orders = new List<OrderModel>();
            int ordersCountOnPage = 10;
            IEnumerable<Order> DBOrders = new List<Order>();
            if (!done)
            {
                DBOrders = new BaseDataAccessLayer<Order>().Get(
                    i => i.Deleted == false &&
                        i.UserId == userId &&
                        (i.TrackInfos.OrderByDescending(v => v.ActionDate).FirstOrDefault().CurrentOrderStatus.Status == OrderStatus.Pending.ToString() ||
                        i.TrackInfos.OrderByDescending(v => v.ActionDate).FirstOrDefault().CurrentOrderStatus.Status == OrderStatus.Confirmed.ToString()),
                        d => d.OrderItems.Select(menu => menu.MenuItem.Dinning.DinningCategory),
                        d => d.OrderItems.Select(menu => menu.MenuItem.Dinning.DinningStatus),
                        d => d.OrderItems.Select(menu => menu.MenuItem.CurrentMenuItemStatus),
                        d => d.Address,
                        d => d.TrackInfos.Select(stat => stat.CurrentOrderStatus)
                    );
            }
            else
            {
                DBOrders = new BaseDataAccessLayer<Order>().Get(
                     i => i.Deleted == false &&
                         i.UserId == userId &&
                         (i.TrackInfos.OrderByDescending(v => v.ActionDate).FirstOrDefault().CurrentOrderStatus.Status == OrderStatus.Completed.ToString() ||
                         i.TrackInfos.OrderByDescending(v => v.ActionDate).FirstOrDefault().CurrentOrderStatus.Status == OrderStatus.Aborted.ToString()),
                         d => d.OrderItems.Select(menu => menu.MenuItem.Dinning.DinningCategory),
                         d => d.OrderItems.Select(menu => menu.MenuItem.Dinning.DinningStatus),
                         d => d.OrderItems.Select(menu => menu.MenuItem.CurrentMenuItemStatus),
                         d => d.Address,
                         d => d.TrackInfos.Select(stat => stat.CurrentOrderStatus)
                     );
            }
            var ordersTemp = DBOrders.ToList();
            ordersTemp.Reverse();
            for (int i = ordersLoaded; i < ordersLoaded + ordersCountOnPage; i++)
            {
                if (i < ordersTemp.Count)
                {
                    orders.Add(CreateOrderViewModel(ordersTemp[i]));
                    if (i == ordersTemp.Count - 1)
                    {
                        AllOrdersLoaded = true;
                    }
                    else
                    {
                        AllOrdersLoaded = false;
                    }
                }
            }
            return orders;
        }

        /// <summary>
        /// Method for adding new order
        /// </summary>
        /// <param name="userId">current user id</param>
        /// <param name="addressId">current user address id </param>
        /// <param name="returnNewOrderData">if client need information about new order - set true, else - set false</param>
        /// <param name="itemsList">contain current order items</param>
        /// <returns>new order information or null</returns>
        public OrderModel AddNewOrder(int userId, int addressId, bool returnNewOrderData, List<OrderItemModel> itemsList)
        {
            var addressDal = new BaseVirtualDataAccessLayer<Address>();
            if (addressDal.Get(i => i.Id == addressId && i.Deleted == false).Any())
            {
                try
                {
                    var newOrder = new Order
                    {
                        AddressId = addressId,
                        UserId = userId,
                        Deleted = false
                    };
                    var newTrack = new TrackInfo
                    {
                        ActionDate = DateTime.Now,
                        ActionInfo = "Order was created by user",
                        CurrentOrderStatusId =
                            new OrderStatusDataAccessLayer().GetStatusByName(OrderStatus.Pending.ToString()).Id
                    };
                    newOrder.TrackInfos.Add(newTrack);
                    foreach (OrderItemModel item in itemsList)
                    {
                        var newOrderItem = new OrderItem();
                        newOrderItem.Deleted = false;
                        newOrderItem.Count = item.Count;
                        newOrderItem.MenuItemId = item.Id;
                        newOrderItem.ItemPrice = new BaseVirtualDataAccessLayer<MenuItem>().GetSingle(i => i.Id == item.Id).Price;
                        newOrder.OrderItems.Add(newOrderItem);
                    }
                    new BaseDataAccessLayer<Order>().Add(newOrder);
                    Message = "New order was added";
                    if (returnNewOrderData)
                    {
                        var order = new BaseDataAccessLayer<Order>().GetSingle(
                               i => i.Id == newOrder.Id,
                                   d => d.OrderItems.Select(menu => menu.MenuItem.Dinning.DinningCategory),
                                   d => d.OrderItems.Select(menu => menu.MenuItem.Dinning.DinningStatus),
                                   d => d.OrderItems.Select(menu => menu.MenuItem.CurrentMenuItemStatus),
                                   d => d.Address,
                                   d => d.TrackInfos.Select(stat => stat.CurrentOrderStatus)
                            );
                        return CreateOrderViewModel(order);
                    }
                    return null;
                }
                catch (Exception)
                {
                    OperationError("New order was not added");
                    return null;
                }
            }
            OperationError("This addresses was not found");
            return null;
        }
        
        /// <summary>
        /// This method convert order entity with relevant data to view model (OrderModel)
        /// </summary>
        /// <param name="order">order entity model, which must be converted</param>
        /// <returns>order view model</returns>
        public OrderModel CreateOrderViewModel(Order order)
        {
            var orderItems = new List<UserOrderItem>();
            double totelPrice = 0;
            foreach (var item in order.OrderItems)
            {
                totelPrice += item.ItemPrice * item.Count;
                orderItems.Add(new UserOrderItem
                {
                    ItemName = item.MenuItem.Name,
                    Count = item.Count,
                    Price = item.ItemPrice,
                    ItemAvailable = !item.MenuItem.Deleted && !item.MenuItem.Dinning.Deleted && item.MenuItem.CurrentMenuItemStatus.Status && item.MenuItem.Dinning.DinningStatus.Status == "Enabled",
                    Description = item.MenuItem.Description,
                    MenuItemId = item.MenuItem.Id
                });
            }
            var trackInfoList = new List<OrderTrackInfo>();
            foreach (var item in order.TrackInfos)
            {
                trackInfoList.Add(new OrderTrackInfo
                {
                    ActionDate = item.ActionDate.ToString(),
                    ActionInfo = item.ActionInfo
                });
            }
            trackInfoList.Sort((x, y) => y.ActionDate.CompareTo(x.ActionDate));
            return new OrderModel
            {
                Id = order.Id,
                DinningName = order.OrderItems.FirstOrDefault().MenuItem.Dinning.Name,
                OrderStaus = order.TrackInfos.OrderByDescending(v => v.ActionDate).FirstOrDefault().CurrentOrderStatus.Status,
                Date = order.TrackInfos.OrderByDescending(v => v.ActionDate).FirstOrDefault().ActionDate.ToString(),
                UserAddress = order.Address.AddressInfo,
                AddressDeleted = order.Address.Deleted,
                TotalPrice = totelPrice,
                OrderItems = orderItems,
                AddressId = order.AddressId,
                DinningId = order.OrderItems.FirstOrDefault().MenuItem.Dinning.Id,
                DinningAvailable = order.OrderItems.FirstOrDefault().MenuItem.Dinning.DinningStatus.Status == DinningStatuses.Enabled,
                TrackInfoList = trackInfoList
            };
        }

        /// <summary>
        /// Method for reopen old order 
        /// </summary>
        /// <param name="orderId">order identificator</param>
        /// <returns>list of order items</returns>
        public List<CartItemModel> ReopenOrder(int orderId)
        {
            var order = new BaseDataAccessLayer<Order>().Get(o => o.Id == orderId,
                o => o.OrderItems,
                o => o.OrderItems.Select(oi => oi.MenuItem.CurrentMenuItemStatus),
                o => o.OrderItems.Select(oi => oi.MenuItem.DishCategory))
                .FirstOrDefault();
            if (order != null && !order.Deleted)
            {
                try
                {
                    var orderItems = new List<CartItemModel>();
                    foreach (var it in order.OrderItems)
                    {
                        if (it.MenuItem.Deleted || !it.MenuItem.CurrentMenuItemStatus.Status)
                            continue;
                        var item = new CartItemModel
                        {
                            Id = it.MenuItemId,
                            Name = it.MenuItem.Name,
                            Price = it.MenuItem.Price.ToString(),
                            Count = it.Count
                        };
                        orderItems.Add(item);
                    }
                    return orderItems;
                }
                catch (Exception)
                {
                    OperationError("Failed to reopen order");
                    return null;
                }
            }
            OperationError("This order was not found!");
            return null;
        }
    }
}
