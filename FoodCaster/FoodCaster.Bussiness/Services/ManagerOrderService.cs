﻿using System;
using System.Collections.Generic;
using System.Linq;
using FoodCaster.Bussiness.Common.Enums;
using FoodCaster.Bussiness.Models;
using FoodCaster.Data.DataAccessLayer;
using FoodCaster.Data.Entities;

namespace FoodCaster.Bussiness.Services
{
    /// <summary>
    /// Service class, represents functionality to work with orders for managers.
    /// </summary>
    public class ManagerOrderService : BaseService<ManagerOrderModel, Order, BaseVirtualDataAccessLayer<Order>>
    {
        /// <summary>
        /// This method return list of order for manager by (string) order status
        /// </summary>
        /// <param name="orderStatus">Pending , Confirmed , Aborted or Completed</param>
        /// <returns>list of order models for manager</returns>
        public List<ManagerOrderModel> GetOrder(string orderStatus = null)
        {
            var orders = DataAccessLayer.Get(
            i => i.OrderItems.Any() &&
                 (i.TrackInfos.OrderByDescending(v => v.ActionDate).FirstOrDefault().CurrentOrderStatus.Status == orderStatus || string.IsNullOrEmpty(orderStatus)),
            d => d.OrderItems.Select(menu => menu.MenuItem.Dinning),
            d => d.User,
            d => d.TrackInfos.Select(status => status.CurrentOrderStatus),
            d => d.Address);

            return orders.Select(order => new ManagerOrderModel
            {
                Id = order.Id,
                UserName = order.User.UserName,
                RestaurantName = order.OrderItems.FirstOrDefault().MenuItem.Dinning.Name,
                Status = order.TrackInfos.OrderByDescending(v => v.ActionDate).FirstOrDefault().CurrentOrderStatus.Status,
                UserAddress = order.Address.AddressInfo,
            }).ToList();
        }

        /// <summary>
        /// This method list of order item for current order by Id
        /// </summary>
        /// <param name="orderId">Current order id</param>
        /// <returns>list of order item models</returns>
        public List<OrderItemModel> GetItemList(int orderId)
        {
            var orderItemDal = new BaseDataAccessLayer<OrderItem>();
            var items = orderItemDal.Get(
                i => i.Order.Id == orderId,
                d => d.MenuItem,
                d => d.Order);

            return items.Select(itm => new OrderItemModel
            {
                Id = itm.Id,
                OrdId = itm.Order.Id,
                Name = itm.MenuItem.Name,
                Count = itm.Count,
                Price = itm.ItemPrice
            }).ToList();
        }
       
        /// <summary>
        /// This method adds new track info for current order
        /// </summary>
        /// <param name="orderId">Current order id</param>
        /// <param name="orderStatus">Current status for add</param>
        /// <param name="updateBy">The one who added new data</param>
        public void AddOrderTrackInfo(int orderId, OrderStatus orderStatus,string updateBy="manager")
        {
            var order = DataAccessLayer.GetSingle(i => i.Id == orderId,
                i => i.TrackInfos.Select(status => status.CurrentOrderStatus));
            var trackInfo = order.TrackInfos.OrderByDescending(v => v.ActionDate).FirstOrDefault();
            if (trackInfo == null) return;
            var orderStatusDal = new BaseDataAccessLayer<CurrentOrderStatus>();
            var trackInfoDal = new BaseDataAccessLayer<TrackInfo>();

            var newtrack = new TrackInfo();
            newtrack.OrderId = trackInfo.OrderId;
            newtrack.ActionDate = DateTime.Now;
            newtrack.ActionInfo = string.Format("Order was {0} by {1}", orderStatus.ToString().ToLower(), updateBy);          
            newtrack.CurrentOrderStatusId = orderStatusDal.GetSingle(i => string.Equals(i.Status.Trim(), orderStatus.ToString())).Id;          
            trackInfoDal.Add(newtrack);
            OperationSuccess(newtrack.ActionInfo);
        }
    }
}
