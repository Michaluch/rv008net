﻿using FoodCaster.Bussiness.Common.Enums;
using FoodCaster.Bussiness.Models;
using FoodCaster.Bussiness.Validation;
using FoodCaster.Data.DataAccessLayer;
using FoodCaster.Data.Entities;

namespace FoodCaster.Bussiness.Services
{
    public interface IBaseService<TModel, TEntity, TRepository> : IValidator<TModel>
        where TModel : BaseModel
        where TEntity : BaseEntity
        where TRepository : BaseDataAccessLayer<TEntity>, new()
    {
        //true if operation was successful
        bool Success { get; set; }
        //error or success message for user
        string Message { get; set; }
        //Method for create entity model-based. Should be overridden in child
        TEntity CreateEntityFromModel(TModel model);
        //Method for create model entity-based. Should be overridden in child
        TModel CreateModelFromEntity(TEntity entity);
        //Base method for get entity object from data base by id
        TEntity GetEntity(int id);
        //Base method for update, delete and add operations (type of operation is passed by 'operation' parameter)
        TModel SaveChanges(TModel model, OperationType operation);
    }
}
