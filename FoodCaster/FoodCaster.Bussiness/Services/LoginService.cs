﻿using System.Linq;
using FoodCaster.Bussiness.Common.Enums;
using FoodCaster.Bussiness.Models;
using FoodCaster.Data.DataAccessLayer;
using FoodCaster.Data.Entities;

namespace FoodCaster.Bussiness.Services
{
    /// <summary>
    /// Service class, represents authorization and registration functionality.
    /// </summary>
    public class LoginService : BaseService<LoginModel, User, UserDataAccessLayer>
    {
        private const string Noimage = "/Content/img/Photo/noimage.jpg";

        public override User CreateEntityFromModel(LoginModel model)
        {
            var roleDal = new RoleDataAccessLayer();
            return new User
            {
                UserName = model.userName,
                RoleId = roleDal.GetRoleByName(RoleType.User.ToString()).Id,
                Password = model.passwordHash,
                Email = model.login,
                PhotoLink = Noimage
            };
        }

        public override void OperationValidate(OperationType operation, LoginModel model)
        {
            //If new user data email is already used
            if (operation == OperationType.Add && DataAccessLayer.IsEmailExist(model.login))
            {
                    OperationError(string.Format("Email {0} is already used.", model.login));
            }
        }

        protected override string GetSuccessMessage(OperationType operation, User entity)
        {
            return string.Format("User {0} was registered!", entity.UserName);
        }

        protected override string GetErrorMessage(OperationType operation, User entity)
        {
            return string.Format("User {0} was not registered!", entity.UserName);
        }

        /// <summary>
        /// This method represents authorization functionality
        /// </summary>
        /// <param name="model">Login model with email and password hash</param>
        /// <returns>User entity</returns>
        public User UserAuthorization(LoginModel model)
        {
            model.userName = "ValidName";
            var errors = DataValidate(model);
            if (errors != null && errors.Any())
            {
                OperationError(errors.First().ErrorMessage);
                return null;
            }

            if (DataAccessLayer.UserIdentify(model.login, model.passwordHash))
            {               
                return DataAccessLayer.GetUserByEmail(model.login);
            }

            OperationError("Login or password incorrect");
            return null;
        }
    }
}
