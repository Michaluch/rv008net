﻿using System.Collections.Generic;
using System.Linq;
using FoodCaster.Bussiness.Models;
using FoodCaster.Data.DataAccessLayer;
using FoodCaster.Data.Entities;

namespace FoodCaster.Bussiness.Services
{
    /// <summary>
    /// Service class, represents functionality to work with Statistic for managers.
    /// </summary>
    public class ManagerStatisticService : BaseService<ManagerStatisticModel, Order, BaseVirtualDataAccessLayer<Order>>
    {
        public List<DiningOrdersCount> GetDiningsRate()
        {
            var dinings = new List<DiningOrdersCount>();
            var dBorders = new BaseVirtualDataAccessLayer<Order>().Get(
                i => i.OrderItems.Any(),
                d => d.OrderItems.Select(menu => menu.MenuItem.Dinning));
            foreach (var item in dBorders.GroupBy(v => v.OrderItems.FirstOrDefault().MenuItem.Dinning.Id))
            {
                dinings.Add(new DiningOrdersCount
                {
                    DId = item.Key,
                    Count = item.Count(),
                    RestaurantName = item.FirstOrDefault().OrderItems.FirstOrDefault().MenuItem.Dinning.Name
                });

            }
            return dinings;
        }

        public List<OrdersItemTop> GetDishesRate(string name)
        {
            var dishes = new List<OrdersItemTop>();
            var dBorders = new BaseVirtualDataAccessLayer<OrderItem>().Get(
                i => i.MenuItem.Dinning.Name == name,
                d => d.MenuItem,
                d => d.MenuItem.Dinning);

            foreach (var itm in dBorders.GroupBy(v => v.MenuItem.Name))
            {
                dishes.Add(new OrdersItemTop
                {
                    Count = itm.Count(),
                    Name = itm.Key,
                    DiningId = itm.FirstOrDefault().MenuItem.DinningId
                });

            }
            return dishes;
        }
    }
}
