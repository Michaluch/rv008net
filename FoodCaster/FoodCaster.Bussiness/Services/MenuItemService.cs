﻿using System;
using System.Linq;
using FoodCaster.Bussiness.Common.Enums;
using FoodCaster.Bussiness.Common.StaticStrings;
using FoodCaster.Bussiness.Models;
using FoodCaster.Data.DataAccessLayer;
using FoodCaster.Data.Entities;

namespace FoodCaster.Bussiness.Services
{
    /// <summary>
    /// Service class, represents functionality to work with menu items for managers and users.
    /// </summary>
    public class MenuItemService : BaseService<MenuItemModel, MenuItem, BaseVirtualDataAccessLayer<MenuItem>>
    {
        public override void OperationValidate(OperationType operation, MenuItemModel model)
        {
            if (operation != OperationType.Delete)
            {
                if (model.Price.IndexOf(',') != -1)
                {
                    model.Price = model.Price.Replace(",", ".");
                }
            }
        }

        protected override string GetSuccessMessage(OperationType operation, MenuItem entity)
        {
            return string.Format(base.GetSuccessMessage(operation, entity), entity.Name);
        }

        protected override string GetErrorMessage(OperationType operation, MenuItem entity)
        {
            return string.Format(base.GetErrorMessage(operation, entity), entity.Name);
        }

        public override MenuItem CreateEntityFromModel(MenuItemModel model)
        {
            var dishcategoruDal = new BaseVirtualDataAccessLayer<DishCategory>();
            var currentStatusDal = new BaseDataAccessLayer<CurrentMenuItemStatus>();
            var itemStatusId = model.CurrentStatusId != 0 ? model.CurrentStatusId : currentStatusDal.GetSingle(i => i.Status == (model.CurrentMenuItemsStatus == MenuItemStatuses.Aviable)).Id;
            var dishCategotyId = model.DishCategoryId != 0 ? model.DishCategoryId : dishcategoruDal.GetSingle(i => i.Name == model.DishCategory).Id;

            return new MenuItem
            {
                DinningId = model.DinningId,
                Deleted = false,
                Price = Convert.ToDouble(model.Price),
                Name = model.Name,
                Description = model.Description,
                DishCategoryId = dishCategotyId,
                CurrentMenuItemStatusId = itemStatusId
            };
        }

        public override MenuItemModel CreateModelFromEntity(MenuItem entity)
        {
            var newEntity = DataAccessLayer.GetSingle(i => i.Id == entity.Id, d => d.CurrentMenuItemStatus, d => d.DishCategory);
            return new MenuItemModel
            {
                Id = newEntity.Id,
                Price = newEntity.Price.ToString(),
                Name = newEntity.Name,
                Description = newEntity.Description,
                CurrentMenuItemsStatus = (newEntity.CurrentMenuItemStatus.Status ? MenuItemStatuses.Aviable : MenuItemStatuses.NotAvailable),
                DishCategory = newEntity.DishCategory.Name
            };
        }

        /// <summary>
        /// This method make menuModel for view menu item , menu items statuses and dish categories in client side
        /// </summary>
        /// <param name="dinningId">Id of dinning for search menu items</param>
        /// <param name="getAllCategories">true - get all categories; false - only ready for use</param>
        /// <returns></returns>
        public MenuModel GetItems(int dinningId, bool getAllCategories=false)
        {
            var menuItems = DataAccessLayer.Get(
                i => i.DinningId == dinningId, 
                    d => d.CurrentMenuItemStatus, 
                    d => d.DishCategory).ToList();

            var menuModel = new MenuModel
            {
                MenuItemsList = menuItems.Select(item => new MenuItemModel
                {
                    CurrentMenuItemsStatus = (item.CurrentMenuItemStatus.Status ? MenuItemStatuses.Aviable : MenuItemStatuses.NotAvailable),
                    Description = item.Description,
                    DishCategory = item.DishCategory.Name,
                    Id = item.Id,
                    Name = item.Name,
                    Price = item.Price.ToString()
                }).ToList(),
                DiningId = dinningId,
                DinningName = new BaseVirtualDataAccessLayer<Dinning>().GetSingle(i => i.Id == dinningId).Name,
                MenuItemStatusesList =
                    new BaseDataAccessLayer<CurrentMenuItemStatus>().Get().Select(status => new StatusModel
                    {
                        Id = status.Id,
                        Name = status.Status ? MenuItemStatuses.Aviable : MenuItemStatuses.NotAvailable,
                    }).ToList(),

                DishCategoriesList = getAllCategories ? new BaseVirtualDataAccessLayer<DishCategory>().Get().Select(category => new CategoryModel
                {
                    Id = category.Id,
                    Name = category.Name
                }).ToList() : menuItems.Select(d => d.DishCategory).GroupBy(d => new { d.Id, Name = d.Name }).Select(group => new CategoryModel
                    {
                        Id = group.Key.Id,
                        Name = group.Key.Name
                    }).ToList()
            };
            return menuModel;
        }
    }
}
