﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using FoodCaster.Bussiness.Common.Enums;
using FoodCaster.Bussiness.Models;
using FoodCaster.Data.DataAccessLayer;
using FoodCaster.Data.Entities;

namespace FoodCaster.Bussiness.Services
{
    /// <summary>
    /// Base service class, represents basic functionality to work with models entities and data repositories.
    /// for add, update, and delete data 
    /// TModel - model for operation which inherit BaseModel
    /// TEntity - entity object for operation which inherit BaseEntity
    /// TRepository - DataAccessLayer wich inherit BaseDataAccessLayer if TEntity have no Deleted property or 
    /// BaseVirtualDataAccessLayer if TEntity have Deleted property
    /// </summary>
    public abstract class BaseService<TModel, TEntity, TRepository> : IBaseService<TModel, TEntity, TRepository>
        where TModel : BaseModel
        where TEntity : BaseEntity , new()
        where TRepository : BaseDataAccessLayer<TEntity>, new()
    {
        protected TRepository DataAccessLayer;
        //true if operation was successful
        public bool Success { get; set; }
        //error or success message for user
        public string Message { get; set; }
        protected BaseService()
        {
            Message = "No message";
            Success = true;
            DataAccessLayer = new TRepository();
        }
        //Method for create entity model-based. Should be overridden in child
        public virtual TEntity CreateEntityFromModel(TModel model) 
        {
            throw new NotImplementedException();
        }
        //Method for create model entity-based. Should be overridden in child
        public virtual TModel CreateModelFromEntity(TEntity entity)
        {
            throw new NotImplementedException();
        }
        //Base method for get entity object from data base by id
        public virtual TEntity GetEntity(int id)
        {
            return DataAccessLayer.GetSingle(i => i.Id == id);
        }
        //Base method for get string format of success message. Should be overridden in child for build correct message
        protected virtual string GetSuccessMessage(OperationType operation,TEntity entity)
        {
            var status = string.Empty;
            switch (operation)
            {
                case OperationType.Add:
                    status = "{0} was added.";
                    break;
                case OperationType.Update:
                    status = "{0} was updated.";
                    break;
                case OperationType.Delete:
                    status = "{0} was deleted.";
                    break;
            }
            return status;
        }
        //Base method for get string format of error message. Should be overridden in child for build correct message
        protected virtual string GetErrorMessage(OperationType operation,TEntity entity)
        {
            var status = string.Empty;
            switch (operation)
            {
                case OperationType.Add:
                    status = "{0} was not added! Please try again";
                    break;
                case OperationType.Update:
                    status = "{0} was not updated! Please try again";
                    break;
                case OperationType.Delete:
                    status = "{0} was not deleted! Please try again";
                    break;
            }
            return status;
        }
        //protected method for specify error situation
        protected void OperationError(string message)
        {
            Success = false;
            Message = message;
        }
        //protected method for specify success situation
        protected void OperationSuccess(string message)
        {
            Success = true;
            Message = message;
        }
        //Base method for model validation
        public virtual IEnumerable<ValidationResult> DataValidate(TModel model)
        {
            var errors = new List<ValidationResult>();
            model.Validate(errors);
            return errors;
        }
        //Method for check possibility to make operation. Should be overridden in child for correct check
        public virtual void OperationValidate(OperationType operation, TModel model)
        {
            throw new NotImplementedException();
        }
        //Base method for update, delete and add operations (type of operation is passed by 'operation' parameter)
        public virtual TModel SaveChanges(TModel model, OperationType operation)
        {
            try
            {
                OperationValidate(operation, model);
                if (operation != OperationType.Delete)
                {
                    var errors = DataValidate(model);
                    if (errors != null && errors.Any())
                    {
                        OperationError(errors.First().ErrorMessage);
                    }
                }
                if (!Success) return model;
                var entity = operation == OperationType.Delete
                    ? GetEntity(model.Id)
                    : CreateEntityFromModel(model);
                try
                {
                    switch (operation)
                    {
                        case OperationType.Add:
                            Add(entity);
                            model.Id = entity.Id;
                            break;
                        case OperationType.Update:
                            entity.Id = model.Id;
                            Update(entity);
                            break;
                        case OperationType.Delete:
                            Remove(entity.Id);
                            break;
                    }
                    OperationSuccess(GetSuccessMessage(operation, entity));
                }
                catch (Exception)
                {
                    OperationError("Database error: " + GetErrorMessage(operation, entity));
                }
            }
            catch (NotImplementedException)
            {
                OperationError("Some methods for this operations not implemented");
            }
            catch (Exception)
            {
                OperationError("Can not complete this operation. Please try again!");
            }
            return model;
        }
        //Base method for add new entity
        protected virtual void Add(TEntity entity)
        {
            DataAccessLayer.Add(entity);
        }
        //Base method for update entity in data base
        protected virtual void Update(TEntity entity)
        {     
            DataAccessLayer.Update(entity);
        }
        //Base method for remove entity from data base by id
        protected virtual void Remove(int id)
        {
           DataAccessLayer.Remove(id);
        }
        //Base method for remove entity from data base by entity object
        protected virtual void Remove(TEntity entity)
        {
            DataAccessLayer.Remove(entity);
        }
    }
}