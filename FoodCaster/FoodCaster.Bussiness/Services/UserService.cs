﻿using System.Collections.Generic;
using System.Linq;
using FoodCaster.Bussiness.Common.Enums;
using FoodCaster.Bussiness.Models;
using FoodCaster.Data.DataAccessLayer;
using FoodCaster.Data.Entities;

namespace FoodCaster.Bussiness.Services
{
    /// <summary>
    /// Service class, represents functionality to work with users.
    /// using in Admin,Manager and User controllers
    /// </summary>
    public class UserService : BaseService<UserModel, User, UserDataAccessLayer>
    {
        private const string Noimage = "/Content/img/Photo/noimage.jpg";

        public override void OperationValidate(OperationType operation , UserModel model)
        {    
            //If Admin or user change user data but not change password
            var user = DataAccessLayer.GetSingle(i => i.Id == model.Id);
            if (string.IsNullOrEmpty(model.Password) && operation == OperationType.Update)
            {
                model.Password = user.Password;
            }
            //If addmin change user data( admin can not change user photo)
            if (string.IsNullOrEmpty(model.Photo)) 
            {
                model.Photo = user.PhotoLink;
            }
            //If new user data email is already used
            if (operation != OperationType.Delete && DataAccessLayer.IsEmailExist(model.Email))
            {
                if (operation == OperationType.Add)
                    OperationError(string.Format("Email {0} is already used.", model.Email));
                else
                {
                    if (model.Email != user.Email)
                    {
                        OperationError(string.Format("Email {0} is already used.", model.Email));
                    }
                }
            }

            var rolesDal = new RoleDataAccessLayer();
            Role userRole = rolesDal.GetRoleByName(model.Role);

            if (operation != OperationType.Delete && userRole == null)
            {
                OperationError("No role in database.");
            }

            if (operation == OperationType.Update || operation == OperationType.Delete)
            {
                var admins = DataAccessLayer.Get(i => i.Role.Name == RoleType.Admin.ToString() && i.Deleted == false);
                if (admins.Count() == 1 && DataAccessLayer.GetSingle(i => i.Id == model.Id, d => d.Role).Role.Name == RoleType.Admin.ToString() && model.Role != RoleType.Admin.ToString())
                {
                    OperationError("You can not change role or delete this user! It is the last admin.");
                }
            }    
        }

        protected override string GetSuccessMessage(OperationType operation, User entity)
        {
            return string.Format(base.GetSuccessMessage(operation, entity) , entity.UserName);
        }

        protected override string GetErrorMessage(OperationType operation, User entity)
        {
            return string.Format(base.GetErrorMessage(operation, entity), entity.UserName);
        }

        public override User CreateEntityFromModel(UserModel model)
        {
            var rolesDal = new RoleDataAccessLayer();
            return new User
            {
                UserName = model.UserName,
                RoleId = rolesDal.GetRoleByName(model.Role).Id,
                Password = model.Password,
                Email = model.Email,
                PhotoLink = model.Photo ?? Noimage
            };
        }

        public override UserModel CreateModelFromEntity(User entity)
        {
            var rolesDal = new RoleDataAccessLayer();
            return new UserModel
            {
                Id = entity.Id,
                Email = entity.Email,
                Password = entity.Password,
                UserName = entity.UserName,
                Role = rolesDal.GetSingle(i => i.Id == entity.RoleId).Name,
                Photo = entity.PhotoLink
                
            };
        }

        /// <summary>
        /// This method return List of users for view this list in client side
        /// with possibility find users by model
        /// </summary>
        /// <param name="model">User model</param>
        /// <returns>List of model</returns>
        public IEnumerable<UserModel> GetItems(UserModel model)
        {
            if (model == null) model = new UserModel();
            var users = DataAccessLayer.Get(
                i => i.Deleted == false &&
                (i.Role.Name.Contains(model.Role) || model.Role == null) &&
                (i.Email.ToLower().Contains(model.Email) || model.Email == null) &&
                (i.UserName.ToLower().Contains(model.UserName) || model.UserName == null),
                d => d.Role);
            return users.Select(CreateModelFromEntity).ToList();
        }

        /// <summary>
        /// This method return one user for login edit or view
        /// </summary>
        /// <param name="model">User model</param>
        /// <returns>User model</returns>
        public UserModel GetItem(UserModel model)
        {
            var userEntity = DataAccessLayer.GetSingle(i => i.Email == model.Email,d=>d.Role);
            if (string.IsNullOrEmpty(userEntity.PhotoLink)) userEntity.PhotoLink = Noimage;
            return new UserModel
            {
                Email = userEntity.Email,
                Id = userEntity.Id,
                Photo = userEntity.PhotoLink,
                Role = userEntity.Role.Name,
                UserName = userEntity.UserName
            };
        }

        /// <summary>
        /// This method return roles list for view it in client side
        /// </summary>
        /// <returns>(string) List of roles</returns>
        public List<string> GetRoles()
        {
            var roleDal = new RoleDataAccessLayer();
            var roles = roleDal.Get();
            return roles.Select(role => role.Name).ToList();
        }
    }
}