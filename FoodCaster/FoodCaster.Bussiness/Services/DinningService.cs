﻿using System.Collections.Generic;
using System.Linq;
using FoodCaster.Bussiness.Common.Enums;
using FoodCaster.Bussiness.Common.StaticStrings;
using FoodCaster.Bussiness.Models;
using FoodCaster.Data.DataAccessLayer;
using FoodCaster.Data.Entities;

namespace FoodCaster.Bussiness.Services
{
    /// <summary>
    /// Service class, represents functionality to work with dinnings.
    /// </summary>
    public class DinningService : BaseService<DinningModel, Dinning, BaseVirtualDataAccessLayer<Dinning>>
    {
        private const string Noimage = "/Content/img/noimage.jpg";

        public override void OperationValidate(OperationType operation, DinningModel model)
        {
            if (string.IsNullOrEmpty(model.Logo))
            {
                model.Logo = Noimage;
            }
            var restaurant = DataAccessLayer.GetSingle(i => i.Id == model.Id);
            if (restaurant == null && operation != OperationType.Add)
            {
                OperationError(string.Format("Invalid restaurant id"));
            } 
        }

        protected override string GetSuccessMessage(OperationType operation, Dinning entity)
        {
            return string.Format(base.GetSuccessMessage(operation, entity), entity.Name);
        }

        protected override string GetErrorMessage(OperationType operation, Dinning entity)
        {
            return string.Format(base.GetErrorMessage(operation, entity), entity.Name);
        }

        public override Dinning CreateEntityFromModel(DinningModel model)
        {
            var catgoryDal = new BaseDataAccessLayer<DinningCategory>();
            var statusDal = new BaseDataAccessLayer<DinningStatus>();
            var dinning = new Dinning
            {
                Deleted = false,
                Name = model.Name,
                DinningCategoryId = catgoryDal.GetSingle(i => i.CategoryName == model.Category).Id,
                DinningStatusId = statusDal.GetSingle(i => i.Status == model.Status).Id,
                Location = model.Location,
                Logo = model.Logo
            };
            return dinning;
        }
        public override DinningModel CreateModelFromEntity(Dinning entity)
        {
            var newEntity = DataAccessLayer.GetSingle(i=>i.Id == entity.Id,d=>d.DinningStatus,d=>d.DinningCategory); 
            return new DinningModel
            {  
                Category = newEntity.DinningCategory.CategoryName,
                Logo = entity.Logo,
                Status = newEntity.DinningStatus.Status,
                Id = entity.Id,
                Location = entity.Location,
                Name =entity.Name
            };
        }
        /// <summary>
        /// This method return List of restaurants for display them on client side
        /// </summary>
        /// <param name="getAllDinnings">true - take all restaurants. Used in Manager Controller.
        /// false - take no deleted restaurants with "true" status. Used in User Controller</param>
        /// <returns>(string) List of restaurants</returns>
        public IEnumerable<DinningModel> GetItems(DinningModel model,bool getAllDinnings=false)
        {
            if (model == null) model = new DinningModel();
            var dinnings = getAllDinnings
                ? DataAccessLayer.Get(i =>    
                    (i.Name.ToLower().Contains(model.Name.ToLower()) || string.IsNullOrEmpty(model.Name)) &&
                         (i.Location.ToLower().Contains(model.Location.ToLower()) || string.IsNullOrEmpty(model.Location)) &&
                         (i.DinningCategory.CategoryName == model.Category || string.IsNullOrEmpty(model.Category)) &&
                         (i.DinningStatus.Status == model.Status || string.IsNullOrEmpty(model.Status)))

                : DataAccessLayer.Get(i => 
                    (i.Name.ToLower().Contains(model.Name.ToLower()) || string.IsNullOrEmpty(model.Name)) &&
                         (i.Location.ToLower().Contains(model.Location.ToLower()) || string.IsNullOrEmpty(model.Location)) &&
                         (i.DinningCategory.CategoryName == model.Category || string.IsNullOrEmpty(model.Category)) &&
                         (i.DinningStatus.Status == model.Status || string.IsNullOrEmpty(model.Status)) && 
                         (i.MenuItems.Any(d => !d.Deleted && d.CurrentMenuItemStatus.Status)));

            return dinnings.Select(CreateModelFromEntity).ToList();
        }

        /// <summary>
        /// This method return List of dinning categories for view this list in client side
        /// </summary>
        /// <param name="getAllDinningCategories">true - return all categories, 
        /// false - where there are dinning and possibility to create new order</param>
        /// <returns>(string) List of categories</returns>
        public List<string> GetCategoryList(bool getAllDinningCategories = false)
        {
            var dinningcategoryDal = new BaseVirtualDataAccessLayer<DinningCategory>();
            var list = getAllDinningCategories
                ? dinningcategoryDal.Get().Select(category => category.CategoryName).ToList()
                : dinningcategoryDal.Get(
                    i => i.Dinnings.Any(d => !d.Deleted && d.DinningStatus.Status == DinningStatuses.Enabled && d.MenuItems.Any(t => !t.Deleted && t.CurrentMenuItemStatus.Status)))
                    .Select(category => category.CategoryName).ToList();
            return list;
        }

        /// <summary>
        /// This method return list of dinning statuses
        /// </summary>
        /// <returns>(string) list of dinning categories</returns>
        public List<string> GetStatusList()
        {
            return new List<string>
            {
                DinningStatuses.Enabled,
                DinningStatuses.Disabled
            };
        }
    }
}
