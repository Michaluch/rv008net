﻿using System.Collections.Generic;
using System.Linq;
using FoodCaster.Bussiness.Common.Enums;
using FoodCaster.Bussiness.Models;
using FoodCaster.Data.DataAccessLayer;
using FoodCaster.Data.Entities;

namespace FoodCaster.Bussiness.Services
{
    public class AddressService : BaseService<AddressModel, Address,BaseVirtualDataAccessLayer<Address>>
    {
        public override void OperationValidate(OperationType operation, AddressModel model)
        {
            var address = DataAccessLayer.GetSingle(i => i.Id == model.Id);
            //if user want to edit/remove default address
            if (operation != OperationType.Add)
            {       
                if (address.Default && !model.IsDefault)
                {
                    OperationError("You must have default address!");
                }
            }

            address = DataAccessLayer.GetSingle(i => i.UserId != null && i.UserId == model.UserId && i.AddressInfo == model.Address);
            //if new address is in DB
            if (operation == OperationType.Add)
            {              
                if (address != null)
                {
                    OperationError("This address is already used!");
                }             
            }
            //if edited address is in DB
            if (operation == OperationType.Update)
            {
                if (address != null && address.Id != model.Id)
                {
                    OperationError("This address is already used!");
                }
            }
            //if user want to send "broken data" from firebug, etc
            if (model.IsDefault && model.UserId == null) OperationError("Address, that isn't binded couldn't be default");

            address = DataAccessLayer.GetSingle(i => i.UserId == model.UserId && i.Default);
            //If it's first address - set it as default
            if (operation == OperationType.Add && !model.IsDefault && address==null)
            {
                model.IsDefault = true;
            }
            //if it's not first address and new address is default - reset old default address to not default    
            if (model.IsDefault && Success && (operation != OperationType.Delete) && address != null)
            {                                
                    address.Default = false;
                    if (!DataValidate(model).Any())
                        DataAccessLayer.Update(address);    
            }
        }

        public override Address CreateEntityFromModel(AddressModel model)
        {
            return new Address
            {
                AddressInfo = model.Address,
                Default = model.IsDefault,
                Id = model.Id,
                UserId = model.UserId
            };
        }

        public override AddressModel CreateModelFromEntity(Address entity)
        {
            return new AddressModel
            {
                Address = entity.AddressInfo,
                IsDefault = entity.Default,
                Id = entity.Id,
                UserId = entity.UserId
            };
        }

        protected override string GetSuccessMessage(OperationType operation, Address entity)
        {
            return string.Format(base.GetSuccessMessage(operation, entity), entity.AddressInfo);
        }

        protected override string GetErrorMessage(OperationType operation, Address entity)
        {
            return string.Format(base.GetErrorMessage(operation, entity), entity.AddressInfo);
        }

        /// <summary>
        /// This method return List of Addresses for view this list in client side
        /// </summary>
        /// <param name="userId">To find address by user Id</param>
        /// <returns>List of address models</returns>
        public List<AddressModel> GetItems(int userId)
        {
            var addresses = new List<AddressModel>();
            var addressesModel = new BaseVirtualDataAccessLayer<Address>().Get(i => i.UserId == userId);
            foreach (var addr in addressesModel)
            {
                addresses.Add(CreateModelFromEntity(addr));
            }
            return addresses;
        }
     
    }
}
