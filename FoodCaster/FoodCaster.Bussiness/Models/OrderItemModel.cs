﻿namespace FoodCaster.Bussiness.Models
{
    public class OrderItemModel:BaseModel
    {
        public int OrdId { get; set; }
        public string Name { get; set; }
        public int Count { get; set; }
        public double Price { get; set; }
    }
}
