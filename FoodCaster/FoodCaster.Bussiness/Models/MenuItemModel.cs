﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FoodCaster.Bussiness.Models
{
    public class MenuItemModel:BaseModel
    {
        [Required(ErrorMessage = "Price is required.")]
        [RegularExpression(@"^[0-9]{1,5}[\.\,]{0,1}[0-9]{0,2}$", ErrorMessage = "Invalid price")]
        public string Price { get; set; }
        [Required(ErrorMessage = "Menu item name is required.")]
        [RegularExpression(@"^[a-zA-Z\s\.\,\'\-\(\)\:]{1,50}$", ErrorMessage = "Invalid menu item name")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Menu item name is required.")]
        [RegularExpression(@"^[a-zA-Z0-9\s\.\,\'\!\-\(\)\:\&]{1,400}$", ErrorMessage = "Invalid menu item name")]
        public string Description { get; set; }
        public string DishCategory { get; set; }
        public string CurrentMenuItemsStatus { get; set; }
        public int DishCategoryId { get; set; }
        public int CurrentStatusId { get; set; }
        public int DinningId { get; set; }
    }
    public class CategoryModel:BaseModel
    {
        public string Name { get; set; }
    }

    public class StatusModel:BaseModel
    {
        public string Name { get; set; }
    }
    public class MenuModel
    {
        public int DiningId { get; set; }
        public string DinningName { get; set; }
        public List<MenuItemModel> MenuItemsList { get; set; }
        public List<CategoryModel> DishCategoriesList { get; set; }
        public List<StatusModel> MenuItemStatusesList { get; set; }
    }
}
