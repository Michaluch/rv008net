﻿using System.Collections.Generic;

namespace FoodCaster.Bussiness.Models
{
    public class OrderModel : BaseModel
    {
        public string DinningName { get; set; }
        public bool DinningAvailable { get; set; }
        public int DinningId { get; set; }
        public string OrderStaus { get; set; }
        public string Date { get; set; }
        public string UserAddress { get; set; }
        public int AddressId { get; set; }
        public bool AddressDeleted { get; set; }
        public double TotalPrice { get; set; }
        public List<UserOrderItem> OrderItems { get; set; }
        public List<OrderTrackInfo> TrackInfoList { get; set; }
    }
    public class UserOrderItem
    {
        public int MenuItemId { get; set; }
        public string ItemName { get; set; }
        public int Count { get; set; }
        public double Price { get; set; }
        public bool ItemAvailable { get; set; }
        public string Description { get; set; }
    }

    public class OrderTrackInfo
    {
        public string ActionDate { get; set; }
        public string ActionInfo { get; set; }
    }
}
