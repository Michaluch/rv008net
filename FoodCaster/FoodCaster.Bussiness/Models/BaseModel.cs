﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FoodCaster.Bussiness.Models
{
    public abstract class BaseModel
    {
        public int Id { get; set; }

        /// <summary>
        /// Validates each property for BaseModel object.
        /// </summary>
        /// <param name="results">ICollection of ValidationResult objects to hold validation results.</param>
        /// <returns>True if the given entity is valid; otherwise, false.</returns>
        public virtual bool Validate(ICollection<ValidationResult> results = null)
        {
            var ctx = new ValidationContext(this, null, null);
            return Validator.TryValidateObject(this, ctx, results, true);
        }
    }
}