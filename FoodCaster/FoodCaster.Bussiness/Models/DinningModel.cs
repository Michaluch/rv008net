﻿using System.ComponentModel.DataAnnotations;

namespace FoodCaster.Bussiness.Models
{
    public class DinningModel:BaseModel
    {
        [Required(ErrorMessage = "Dinning name is required.")]
        [RegularExpression(@"^[a-zA-Z0-9'\s]{1,25}$", ErrorMessage = "Invalid dinning name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Location is required.")]
        [RegularExpression(@"^[a-zA-Z0-9,-/'\s]{1,25}$", ErrorMessage = "Invalid location")]
        public string Location { get; set; }

        [Required(ErrorMessage = "Category is required.")]
        public string Category { get; set; }

        [Required(ErrorMessage = "Status is required.")]
        public string Status { get; set; }

        [RegularExpression(@"^[a-zA-Z0-9,-/'\s]{1,50}$", ErrorMessage = "Invalid dinning logo")]
        public string Logo { get; set; }
    }
}
