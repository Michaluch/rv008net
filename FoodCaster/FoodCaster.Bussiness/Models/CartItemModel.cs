﻿namespace FoodCaster.Bussiness.Models
{
    public class CartItemModel:BaseModel
    {
        public string Price { get; set; }
        public string Name { get; set; }
        public int Count { get; set; }
    }
}
