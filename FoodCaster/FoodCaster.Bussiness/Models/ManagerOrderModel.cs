﻿namespace FoodCaster.Bussiness.Models
{
    public class ManagerOrderModel:BaseModel
    {
        public string UserName { get; set; }
        public string RestaurantName { get; set; }
        public string UserAddress { get; set; }
        public string Status { get; set; }
    }
}
