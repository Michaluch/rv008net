﻿using System.ComponentModel.DataAnnotations;

namespace FoodCaster.Bussiness.Models
{
    public class UserModel : BaseModel
    {
        [Required(ErrorMessage = "User name is required.")]
        [RegularExpression(@"^[A-z][A-z|\.|\s]{1,25}$", ErrorMessage = "Invalid username")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "User email is required.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "User password is required.")]
        [RegularExpression(@"^[a-fA-F0-9]{128}$", ErrorMessage = "Invalid password hash")]
        public string Password { get; set; }

        [Required(ErrorMessage = "User role is required.")]
        [RegularExpression(@"^[a-zA-Z0-9]{1,10}$", ErrorMessage = "Invalid role")]
        public string Role { get; set; }
        public string Photo { get; set; }
    }
}