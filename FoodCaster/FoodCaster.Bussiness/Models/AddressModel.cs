﻿using System.ComponentModel.DataAnnotations;

namespace FoodCaster.Bussiness.Models
{
    public class AddressModel : BaseModel
    {
        public int? UserId { get; set; }
        [Required(ErrorMessage = "Address is required.")]
        [RegularExpression(@"^[a-zA-Z0-9,-/'\s]{1,25}$", ErrorMessage = "Invalid address")]
        public string Address { get; set; }
        public bool IsDefault { get; set; }
    }
}
