﻿using System.ComponentModel.DataAnnotations;

namespace FoodCaster.Bussiness.Models
{
    public class LoginModel : BaseModel
    {
        [Required(ErrorMessage = "User name is required.")]
        [RegularExpression(@"^[A-z][A-z|\.|\s]{1,25}$", ErrorMessage = "Invalid username")]
        public string userName { get; set; }

        [Required(ErrorMessage = "User email is required.")]
        [RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessage = "E-mail is not valid")]
        public string login { get; set; }

        [Required(ErrorMessage = "User password is required.")]
        [RegularExpression(@"^[a-fA-F0-9]{128}$", ErrorMessage = "Invalid password hash")]
        public string passwordHash { get; set; }

    }
}
