﻿using System.Collections.Generic;

namespace FoodCaster.Bussiness.Models
{
    public class ManagerStatisticModel : BaseModel
    {
        public List<DiningOrdersCount> DiningOrdersCountList { get; set; }
        public List<OrdersItemTop> OrdersItemTopList { get; set; }
    }

    public class DiningOrdersCount
    {
        public int DId { get; set; }
        public string RestaurantName { get; set; }
        public int Count { get; set; }
    }

    public class OrdersItemTop
    {
        public int Count { get; set; }
        public string Name { get; set; }
        public int DiningId { get; set; }
    }

}