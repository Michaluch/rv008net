﻿using System.Data.Entity;
using FoodCaster.Data.Entities;

namespace FoodCaster.Data
{
    public class DbModelContext : DbContext
    {
        public DbModelContext()
            : this("name=FoodCasterDb")
        {
        }

        public DbModelContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<DbModelContext>());
        }

        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<Address> Addresses { get; set; }
        public virtual DbSet<OrderItem> OrderItems { get; set; }
        public virtual DbSet<DishCategory> DishCategories { get; set; }
        public virtual DbSet<MenuItem> MenuItems { get; set; }
        public virtual DbSet<CurrentOrderStatus> CurrentOrderStatuses { get; set; }
        public virtual DbSet<DinningCategory> DinningCategories { get; set; }
        public virtual DbSet<Dinning> Dinnings { get; set; }
        public virtual DbSet<TrackInfo> TrackInfos { get; set; }
        public virtual DbSet<CurrentMenuItemStatus> CurrentMenuItemStatuses { get; set; }
        public virtual DbSet<DinningStatus> DinningStatuses { get; set; }
    }
}
