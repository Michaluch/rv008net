﻿using System.Collections.Generic;

namespace FoodCaster.Data.Entities
{
    public class DinningCategory : BaseVirtualEntity
    {
        public DinningCategory()
        {
            Dinnings = new HashSet<Dinning>();
        }

        public string CategoryName { get; set; }

        public virtual ICollection<Dinning> Dinnings { get; set; }
    }
}
