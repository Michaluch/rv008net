﻿using System.Collections.Generic;

namespace FoodCaster.Data.Entities
{
    public class DinningStatus : BaseEntity
    {
        public DinningStatus()
        {
            Dinnings = new HashSet<Dinning>();
        }

        public string Status { get; set; }

        public virtual ICollection<Dinning> Dinnings { get; set; }
    }
}
