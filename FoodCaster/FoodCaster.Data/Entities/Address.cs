﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace FoodCaster.Data.Entities
{
    public class Address : BaseVirtualEntity
    {
        public Address()
        {
            Orders = new HashSet<Order>();
        }

        public string AddressInfo { get; set; }

        public bool Default { get; set; }

        [ForeignKey("User")]
        public int? UserId { get; set; }

        public virtual ICollection<Order> Orders { get; set; }

        public virtual User User { get; set; }
    }
}
