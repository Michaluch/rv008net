﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace FoodCaster.Data.Entities
{
    public class User : BaseVirtualEntity
    {
        public User()
        {
            Orders = new HashSet<Order>();
            Addresses = new HashSet<Address>();
        }

        public string UserName { get; set; }
        
        public string Password { get; set; }
        
        public string Email { get; set; }

        [ForeignKey("Role")]
        public int RoleId { get; set; }
        
        public string PhotoLink { get; set; }

        public virtual Role Role { get; set; }
        
        public virtual ICollection<Order> Orders { get; set; }        
        
        public virtual ICollection<Address> Addresses { get; set; }
    }
}
