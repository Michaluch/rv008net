﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace FoodCaster.Data.Entities
{
    public class Dinning : BaseVirtualEntity
    {
        public Dinning()
        {
            MenuItems = new HashSet<MenuItem>();
        }

        public string Name { get; set; }

        public string Location { get; set; }

        [ForeignKey("DinningCategory")]
        public int DinningCategoryId { get; set; }

        public string Logo { get; set; }

        [ForeignKey("DinningStatus")]
        public int DinningStatusId { get; set; }

        public virtual DinningCategory DinningCategory { get; set; }

        public virtual ICollection<MenuItem> MenuItems { get; set; }

        public virtual DinningStatus DinningStatus { get; set; }
    }
}
