﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace FoodCaster.Data.Entities
{
    public class TrackInfo : BaseEntity
    {
        public DateTime ActionDate { get; set; }
        
        public string ActionInfo { get; set; }

        [ForeignKey("Order")]
        public int OrderId { get; set; }

        [ForeignKey("CurrentOrderStatus")]
        public int CurrentOrderStatusId { get; set; }

        public virtual Order Order { get; set; }
        
        public virtual CurrentOrderStatus CurrentOrderStatus { get; set; }
    }
}
