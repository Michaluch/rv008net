﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace FoodCaster.Data.Entities
{
    public class MenuItem : BaseVirtualEntity
    {
        public MenuItem()
        {
            OrderItems = new HashSet<OrderItem>();
        }

        public double Price { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        [ForeignKey("DishCategory")]
        public int DishCategoryId { get; set; }

        [ForeignKey("Dinning")]
        public int DinningId { get; set; }

        [ForeignKey("CurrentMenuItemStatus")]
        public int CurrentMenuItemStatusId { get; set; }

        public virtual ICollection<OrderItem> OrderItems { get; set; }

        public virtual DishCategory DishCategory { get; set; }

        public virtual Dinning Dinning { get; set; }

        public virtual CurrentMenuItemStatus CurrentMenuItemStatus { get; set; }
    }
}
