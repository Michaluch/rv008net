﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FoodCaster.Data.Entities
{
    public class OrderItem : BaseVirtualEntity
    {
        public int Count { get; set; }
        
        public double ItemPrice { get; set; }

        [ForeignKey("Order")]
        public int OrderId { get; set; }

        [ForeignKey("MenuItem")]
        public int MenuItemId { get; set; }

        public virtual Order Order { get; set; }

        public virtual MenuItem MenuItem { get; set; }
    }
}
