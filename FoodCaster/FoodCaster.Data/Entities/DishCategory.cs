﻿using System.Collections.Generic;

namespace FoodCaster.Data.Entities
{
    public class DishCategory : BaseVirtualEntity
    {
        public DishCategory()
        {
            MenuItems = new HashSet<MenuItem>();
        }

        public string Name { get; set; }

        public virtual ICollection<MenuItem> MenuItems { get; set; }
    }
}
