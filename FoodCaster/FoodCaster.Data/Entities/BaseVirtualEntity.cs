﻿namespace FoodCaster.Data.Entities
{
    public abstract class BaseVirtualEntity : BaseEntity
    {
        public bool Deleted { get; set; }
    }
}
