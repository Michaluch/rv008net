﻿using System.Collections.Generic;

namespace FoodCaster.Data.Entities
{
    public class CurrentMenuItemStatus : BaseEntity
    {
        public CurrentMenuItemStatus()
        {
            MenuItems = new HashSet<MenuItem>();
        }

        public bool Status { get; set; }

        public virtual ICollection<MenuItem> MenuItems { get; set; }
    }
}
