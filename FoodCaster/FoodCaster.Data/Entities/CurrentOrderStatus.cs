﻿using System.Collections.Generic;

namespace FoodCaster.Data.Entities
{
    public class CurrentOrderStatus : BaseEntity
    {
        public CurrentOrderStatus()
        {
            TrackInfos = new HashSet<TrackInfo>();
        }

        public string Status { get; set; }

        public virtual ICollection<TrackInfo> TrackInfos { get; set; }
    }
}
