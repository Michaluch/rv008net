﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace FoodCaster.Data.Entities
{
    public class Order : BaseVirtualEntity
    {
        public Order()
        {
            TrackInfos = new HashSet<TrackInfo>();
            OrderItems = new HashSet<OrderItem>();
        }

        [ForeignKey("User")]
        public int UserId { get; set; }

        [ForeignKey("Address")]
        public int AddressId { get; set; }

        public virtual ICollection<TrackInfo> TrackInfos { get; set; }

        public virtual User User { get; set; }

        public virtual Address Address { get; set; }

        public virtual ICollection<OrderItem> OrderItems { get; set; }
    }
}
