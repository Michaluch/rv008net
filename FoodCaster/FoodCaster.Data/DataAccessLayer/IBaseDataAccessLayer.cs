﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using FoodCaster.Data.Entities;

namespace FoodCaster.Data.DataAccessLayer
{
    /// <summary>
    /// Base IDataAccessLayer interface
    /// </summary>
    public interface IBaseDataAccessLayer<TEntity> where TEntity : BaseEntity
    {
        /// <summary>
        /// This is a basic generic method for adding a new TEntity object to the database where TEntity : BaseEntity.
        /// </summary>
        /// <param name="entity">Entity object with generic type TEntity where TEntity : BaseEntity.</param>
        void Add(TEntity entity);

        /// <summary>
        /// This is a basic generic method for phisical removing records from the database by id.
        /// </summary>
        /// <param name="id">Identification number of record in database.</param>
        void Remove(object id);

        /// <summary>
        /// This is a basic generic method for phisical removing record from the database 
        /// by specified type of entity
        /// </summary>
        /// <param name="entity">Entity object with generic type TEntity where TEntity : class.</param>
        void Remove(TEntity entity);

        /// <summary>
        /// This is a basic generic method for update record in the database bythe given TEntity object where TEntity : BaseEntity.
        /// </summary>
        /// <param name="entity">TEntity object where TEntity : BaseEntity.</param>
        void Update(TEntity entity);

        /// <summary>
        /// This is a basic generic method for get list of entities from the database 
        /// by specified type of entity, lambda expresion and expresion of navigation property for include.
        /// </summary>
        /// <param name="predicate">Expression for choice data from data base.</param>
        /// <param name="includeProperties">Expression for choice list of included properties.</param>
        /// <returns>Collection of entities by the specified predicate with included properties.</returns>
        IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> predicate = null,
            params Expression<Func<TEntity, object>>[] includeProperties);

        /// <summary>
        /// This is a basic generic method for get only one entity from the database 
        /// by specified type of entity, lambda expresion and expresion of navigation property for include.
        /// </summary>
        /// <param name="predicate">Expression for choice data from data base.</param>
        /// <param name="includeProperties">Expression for choice list of included properties.</param>
        /// <returns>One entity object by the specified predicate with included properties.</returns>
        TEntity GetSingle(Expression<Func<TEntity, bool>> predicate = null, params Expression<Func<TEntity, object>>[] includeProperties);

    }
}
