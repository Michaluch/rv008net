﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using FoodCaster.Data.Entities;

namespace FoodCaster.Data.DataAccessLayer
{
    /// <summary>
    /// DataAccessLayer class, represents basic functionality to work with entities 
    /// whose status deleted = false 
    /// </summary>
    public class BaseVirtualDataAccessLayer<TVirtualEntity> : BaseDataAccessLayer<TVirtualEntity>
        where TVirtualEntity : BaseVirtualEntity
    {
        /// <summary>
        /// Method for no phisical delete entity from the database.
        /// </summary>
        /// <param name="entity">Entity for delete.</param>
        public override void Remove(TVirtualEntity entity)
        {
            entity.Deleted = true;
            base.Update(entity);
        }
        /// <summary>
        /// Method for no phisical removing records from the database by id.
        /// </summary>
        /// <param name="id">Identification number of record in database.</param>
        public override void Remove(object id)
        {
            using (var context = new DbModelContext())
            {
                var entity = context.Set<TVirtualEntity>().Find(id);
                entity.Deleted = true;
                context.SaveChanges();
            }
        }
        /// <summary>
        /// Method for get list of entities from the database whose status deleted = false 
        /// by specified type of entity, lambda expresion and expresion of navigation property for include.
        /// </summary>
        /// <param name="predicate">Expression for choice data from data base.</param>
        /// <param name="includeProperties">Expression for choice list of included properties.</param>
        /// <returns>Collection of entities by the specified predicate with included properties.</returns>
        public override IEnumerable<TVirtualEntity> Get(Expression<Func<TVirtualEntity, bool>> predicate = null, params Expression<Func<TVirtualEntity, object>>[] includeProperties)
        {
            using (var context = new DbModelContext())
            {
                IQueryable<TVirtualEntity> query = context.Set<TVirtualEntity>();

                if (predicate != null)
                {
                    query = query.Where(predicate);
                }

                if (includeProperties == null)
                    return query.ToList();

                query = includeProperties.Aggregate(query,
                    (current, navigationProperty) => current.Include(navigationProperty));
                return query.Where(i => i.Deleted == false).ToList();
            }
        }
    }
}
