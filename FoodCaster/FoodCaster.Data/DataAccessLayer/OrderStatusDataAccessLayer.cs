﻿using FoodCaster.Data.Entities;

namespace FoodCaster.Data.DataAccessLayer
{
    /// <summary>
    /// Data access layer class for OrderStatus entities, using genric repository patern
    /// This class inherit basic DataAccessLayer
    /// </summary>
    public class OrderStatusDataAccessLayer : BaseDataAccessLayer<CurrentOrderStatus>
    {
        /// <summary>
        /// Method for get one OrderStatus from the data base by status name
        /// without navigation properties
        /// </summary>
        /// <param name="name">Status name as string</param>
        /// <returns>One Order status entity</returns>
        public CurrentOrderStatus GetStatusByName(string name)
        {
            return GetSingle(i => i.Status.TrimEnd() == name);
        }
    }
}