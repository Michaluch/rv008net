﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using FoodCaster.Data.Entities;

namespace FoodCaster.Data.DataAccessLayer
{
    /// <summary>
    /// DataAccessLayer class, represents basic functionality to work with database.
    /// </summary>
    public class BaseDataAccessLayer<TEntity> : IBaseDataAccessLayer<TEntity> where TEntity : BaseEntity
    {
        public virtual void Add(TEntity entity)
        {
            using (var context = new DbModelContext())
            {
                context.Set<TEntity>().Add(entity);
                context.SaveChanges();
            }
        }

        public virtual void Remove(object id)
        {
            using (var context = new DbModelContext())
            {
                var entity = context.Set<TEntity>().Find(id);
                context.Set<TEntity>().Remove(entity);
                context.SaveChanges();
            }
        }

        public virtual void Remove(TEntity newEntity)
        {
            using (var context = new DbModelContext())
            {
                context.Set<TEntity>().Attach(newEntity);
                context.Set<TEntity>().Remove(newEntity);
                context.SaveChanges();
            }
        }

        public virtual void Update(TEntity newEntity)
        {
            using (var context = new DbModelContext())
            {
                context.Set<TEntity>().Attach(newEntity);
                context.Entry(newEntity).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public virtual IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> predicate = null,
            params Expression<Func<TEntity, object>>[] includeProperties)
        {
            using (var context = new DbModelContext())
            {
                IQueryable<TEntity> query = context.Set<TEntity>();

                if (predicate != null)
                {
                    query = query.Where(predicate);
                }

                if (includeProperties == null)
                    return query.ToList();

                query = includeProperties.Aggregate(query,
                    (current, navigationProperty) => current.Include(navigationProperty));
                return query.ToList();
            }
        }

        public virtual TEntity GetSingle(Expression<Func<TEntity, bool>> predicate = null,
            params Expression<Func<TEntity, object>>[] includeProperties)
        {
            return Get(predicate, includeProperties).FirstOrDefault();
        }
    }
}