﻿using FoodCaster.Data.Entities;

namespace FoodCaster.Data.DataAccessLayer
{
    /// <summary>
    /// Data access layer class for RolesData entities, using genric repository patern
    /// This class inherit basic DataAccessLayer
    /// </summary>
    public class RoleDataAccessLayer : BaseDataAccessLayer<Role>
    {
        /// <summary>
        /// Method for get one role from the data base by role name
        /// without navigation properties
        /// </summary>
        /// <param name="roleName">Role name as string</param>
        /// <returns>One Role rntity</returns>
        public Role GetRoleByName(string roleName)
        {
            return GetSingle(i => string.Equals(i.Name.Trim(), roleName));
        }
    }
}