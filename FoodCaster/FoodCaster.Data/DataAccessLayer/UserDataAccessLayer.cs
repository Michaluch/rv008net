﻿using System.Linq;
using FoodCaster.Data.Entities;

namespace FoodCaster.Data.DataAccessLayer
{
    /// <summary>
    /// UsersAccessLayer class to work with User entities.
    /// This class inherits basic DataAccessLayer.
    /// </summary>
    public class UserDataAccessLayer : BaseVirtualDataAccessLayer<User>
    {
        /// <summary>
        /// Method for add new user in database by user entity object
        /// </summary>
        /// <param name="user">User entity object</param>
        public override void Add(User user)
        {
            using (var context = new DbModelContext())
            {
                context.Users.Add(user);
                context.SaveChanges();
                context.Entry(user).Reference(i => i.Role).Load();
            }
        }
        /// <summary>
        /// Method for get user from database by user email as string
        /// </summary>
        /// <param name="email">string of email</param>
        /// <returns>user entity object</returns>
        public User GetUserByEmail(string email)
        {
            return GetSingle(
                i =>
                    i.Email.Trim() == email.Trim(), 
                i => i.Orders,
                i => i.Role);
        }
        /// <summary>
        /// Method for check user email availability in database
        /// by string email
        /// </summary>
        /// <param name="email">string of email</param>
        /// <returns>true - if email is exist</returns>
        public bool IsEmailExist(string email)
        {
            return Get(i => i.Email == email).Any();
        }
        /// <summary>
        /// Method for check user UserIdentify by email and password hash
        /// </summary>
        /// <param name="email">string of email</param>
        /// <param name="password">string of pasword hash</param>
        /// <returns>true - if user identified</returns>
        public bool UserIdentify(string email, string password)
        {
            User user = GetSingle(i => i.Email.Trim() == email.Trim());
            if (user == null) return false;
            return user.Password.Trim() == password.Trim();
        }
    } 
}